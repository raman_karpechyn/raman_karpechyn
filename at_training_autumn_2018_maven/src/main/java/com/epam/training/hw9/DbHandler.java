package com.epam.training.hw9;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DbHandler {
    private static final String CON_STR = "jdbc:sqlite:src/main/resources/db/db_for_m4_l3.db";

    public static Employees getAllEmployees() throws ClassNotFoundException {
        Class.forName("org.sqlite.JDBC");
        try (Statement statement = DriverManager.getConnection(CON_STR).createStatement()) {
            Employees temp = new Employees();
            ResultSet resultSet = statement.executeQuery("select * from Employee");

            while (resultSet.next()) {
                temp.add(new Employee(resultSet.getInt("id"),
                        resultSet.getString("firstname"),
                        resultSet.getString("lastname")));
            }

            resultSet.close();

            return temp;
        } catch (SQLException e) {
            e.printStackTrace();
            return new Employees();
        }
    }

    public static void addAllEmployees(Employees employees) throws ClassNotFoundException, SQLException {
        Class.forName("org.sqlite.JDBC");
        Connection connection = DriverManager.getConnection(CON_STR);
        Statement statement = connection.createStatement();
        statement.executeUpdate("delete from Employee");

        for (Employee x : employees.asList()) {
            statement.executeUpdate(String.format("insert into Employee (id,firstname,lastname) values (%s, '%s', '%s')",
                    x.getId(), x.getFirstName(), x.getLastName()));
        }

        statement.close();
        connection.close();
    }
}
