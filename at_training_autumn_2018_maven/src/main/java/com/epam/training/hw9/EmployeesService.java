package com.epam.training.hw9;

public class EmployeesService {
    public static void addEmployee(int id) {
        if (EmployeeSingleton.getInstance().getEmployee(id) != null) {
            System.out.println("Employee with such ID already exist!");
            return;
        }

        System.out.println("Enter Employee FIRST NAME:");
        String fname = ScannerSingleton.getInstance().nextLine();

        System.out.println("Enter Employee LAST NAME:");
        String lname = ScannerSingleton.getInstance().nextLine();

        if (EmployeeSingleton.getInstance().add(new Employee(id, fname, lname))) {
            System.out.println("Employee successfully added.");
        } else {
            System.out.println("Unexpected error occurred. Employee does not added.");
        }
    }

    public static void removeEmployee(int id) {
        if (EmployeeSingleton.getInstance().removeEmployee(id)) {
            System.out.println("Employee successfully removed.");
        } else {
            System.out.println("There is no Employee with such ID.");
        }
    }

    public static void saveEmployeesTableToDb() {
        try {
            DbHandler.addAllEmployees(EmployeeSingleton.getInstance());
            System.out.println("Employees table successfully saved.");
        } catch (Exception e) {
            System.out.println("Unexpected error occurred. Employees table does not saved.");
        }
    }

    public static Employees openEmployeesTableFromDb() {
        Employees employees = new Employees();

        try {
            employees = DbHandler.getAllEmployees();
        } catch (Exception e) {
            System.out.println("Unexpected error occurred. Employees table does not opened.");
        }

        return employees;
    }
}
