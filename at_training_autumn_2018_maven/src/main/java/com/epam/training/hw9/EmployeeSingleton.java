package com.epam.training.hw9;

public class EmployeeSingleton {
    private static Employees employees;

    private EmployeeSingleton() {
    }

    public static Employees getInstance() {
        if (employees == null) {
            employees = EmployeesService.openEmployeesTableFromDb();
        }
        return employees;
    }
}
