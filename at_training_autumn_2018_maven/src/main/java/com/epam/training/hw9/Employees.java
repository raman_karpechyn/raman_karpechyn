package com.epam.training.hw9;

import java.io.Serializable;
import java.util.ArrayList;

public class Employees implements Serializable {
    private ArrayList<Employee> employees;

    public Employees() {
        employees = new ArrayList<>();
    }

    public Employees(ArrayList<Employee> employees) {
        this.employees = employees;
    }

    public boolean add(Employee employee) {
        if (getEmployee(employee.getId()) == null) {
            employees.add(employee);
            return true;
        }
        return false;
    }

    public Employee getEmployee(int id) {
        for (Employee x : employees) {
            if (x.getId() == id) {
                return x;
            }
        }
        return null;
    }

    public boolean removeEmployee(int id) {
        Employee temp = getEmployee(id);
        if (temp != null) {
            employees.remove(temp);
            return true;
        }
        return false;
    }

    public ArrayList<Employee> asList() {
        ArrayList<Employee> temp = new ArrayList<>();
        for (Employee x : employees) {
            temp.add(new Employee(x.getId(), x.getFirstName(), x.getLastName()));
        }
        return temp;
    }

    @Override
    public String toString() {
        StringBuilder temp = new StringBuilder();
        temp.append("ID   |FIRST NAME     |LAST NAME      \n");
        for (Employee x : employees) {
            temp.append(String.format("%s\n", x.toString()));
        }
        return temp.toString();
    }
}
