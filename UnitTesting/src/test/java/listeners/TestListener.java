package listeners;

import org.testng.IInvokedMethod;
import org.testng.IInvokedMethodListener;
import org.testng.ITestResult;

import java.util.Date;

public class TestListener implements IInvokedMethodListener {
    public void beforeInvocation(IInvokedMethod iInvokedMethod, ITestResult iTestResult) {
        System.out.println(String.format("[METHOD_STARTED] - %s on %s", iInvokedMethod.getTestMethod().getMethodName(), new Date(iInvokedMethod.getDate())));
    }

    public void afterInvocation(IInvokedMethod iInvokedMethod, ITestResult iTestResult) {
        System.out.println(String.format("[METHOD_FINISHED] - %s >>> %s", iInvokedMethod.getTestMethod().getMethodName(), iTestResult.getStatus() == 1 ? "Success" : "Fail"));
    }
}
