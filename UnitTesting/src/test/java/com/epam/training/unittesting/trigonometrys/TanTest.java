package com.epam.training.unittesting.trigonometrys;

import com.epam.training.unittesting.CalculatorTest;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TanTest extends CalculatorTest {
    private double value;
    private double badValue;

    public TanTest(double value, double badValue) {
        this.value = value;
        this.badValue = badValue;
    }

    @Test
    public void testTan() {
        double expected = Math.tan(value);
        double actual = calculator.tg(value);
        Assert.assertEquals(actual, expected);
    }

    @Test(expectedExceptions = NumberFormatException.class)
    public void testTanForBadValue() {
        double actual = calculator.tg(badValue);
    }
}
