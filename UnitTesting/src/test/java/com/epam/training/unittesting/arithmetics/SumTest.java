package com.epam.training.unittesting.arithmetics;

import com.epam.training.unittesting.CalculatorTest;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SumTest extends CalculatorTest {

    @Test(dataProvider = "Data for long")
    public void testSumForLong(long one, long two) {
        long expected = one + two;
        long actual = calculator.sum(one, two);
        Assert.assertEquals(actual, expected);
    }

    @Test(dataProvider = "Data for double")
    public void testSumForDouble(double one, double two) {
        double expected = one + two;
        double actual = calculator.sum(one, two);
        Assert.assertEquals(actual, expected);
    }
}
