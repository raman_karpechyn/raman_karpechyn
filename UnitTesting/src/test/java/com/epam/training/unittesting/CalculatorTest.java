package com.epam.training.unittesting;

import com.epam.tat.module4.Calculator;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;

public class CalculatorTest {
    protected static Calculator calculator;

    @BeforeMethod
    public static void setUp() {
        calculator = new Calculator();
    }

    @DataProvider(name = "Data for long")
    public static Object[][] createLongData() {
        return new Object[][]{
                {2L, 3L},
                {-353L, -48330L},
                {-3L, 30L},
                {493L, -8583L}
        };
    }

    @DataProvider(name = "Data for double")
    public static Object[][] createDoubleData() {
        return new Object[][]{
                {0.54d, 403.558d},
                {-33.5d, -39498.948d},
                {-96.11d, 0.99d},
                {2d, -0.948d}
        };
    }
}
