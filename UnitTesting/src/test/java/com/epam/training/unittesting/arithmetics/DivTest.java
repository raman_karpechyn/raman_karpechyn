package com.epam.training.unittesting.arithmetics;

import com.epam.training.unittesting.CalculatorTest;
import org.testng.Assert;
import org.testng.annotations.Test;

public class DivTest extends CalculatorTest {
    @Test(dataProvider = "Data for long")
    public void testDivForLong(long one, long two) {
        long expected = one / two;
        long actual = calculator.div(one, two);
        Assert.assertEquals(actual, expected);
    }

    @Test(dataProvider = "Data for double")
    public void testDivForDouble(double one, double two) {
        double expected = one / two;
        double actual = calculator.div(one, two);
        Assert.assertEquals(actual, expected);
    }

    @Test(expectedExceptions = NumberFormatException.class)
    public void testDivByZeroLong() {
        long actual = calculator.div(1L, 0L);
    }

    @Test(expectedExceptions = NumberFormatException.class)
    public void testDivByZeroDouble() {
        double actual = calculator.div(1d, 0d);
    }
}
