package com.epam.training.unittesting.functions;

import com.epam.training.unittesting.CalculatorTest;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class PowTest extends CalculatorTest {
    @Test
    @Parameters({"positiveDouble", "negativeDouble"})
    public void testPow(double one, double two) {
        double expected = Math.pow(one, two);
        double actual = calculator.pow(one, two);
        Assert.assertEquals(actual, expected);
    }

    @Test(expectedExceptions = NumberFormatException.class)
    @Parameters({"negativeDouble", "positiveDouble"})
    public void testPowForNegativeValue(double one, double two) {
        double actual = calculator.pow(one, two);
    }

    @Test(dependsOnMethods = "testPow")
    public void testPowWithZero() {
        testPow(0d, 1d);
        testPow(1d, 0d);
    }
}
