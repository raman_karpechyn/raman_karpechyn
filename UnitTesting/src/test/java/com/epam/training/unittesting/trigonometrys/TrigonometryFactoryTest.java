package com.epam.training.unittesting.trigonometrys;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;

public class TrigonometryFactoryTest {
    @DataProvider(name = "Data for sin/cos tests")
    public Object[] createSinCosData() {
        return new Object[]{-180d, -39.49d, 0d, 66.4d, 90d, 180d, 270d, 360d};
    }

    @DataProvider(name = "Data for tan tests")
    public Object[][] createTanData() {
        return new Object[][]{
                {-180d, -270d},
                {-39.49d, -90},
                {0d, 90},
                {66.4d, 270}
        };
    }

    @DataProvider(name = "Data for ctan tests")
    public Object[][] createCtanData() {
        return new Object[][]{
                {-127d, -360d},
                {-39.49d, -180},
                {13d, 180},
                {66.4d, 360}
        };
    }

    @Factory(dataProvider = "Data for sin/cos tests")
    public Object[] createSin(double value) {
        return new Object[]{new SinTest(value)};
    }

    @Factory(dataProvider = "Data for sin/cos tests")
    public Object[] createCos(double value) {
        return new Object[]{new CosTest(value)};
    }

    @Factory(dataProvider = "Data for tan tests")
    public Object[] createTan(double value, double badValue) {
        return new Object[]{new TanTest(value, badValue)};
    }

    @Factory(dataProvider = "Data for ctan tests")
    public Object[] createCtan(double value, double badValue) {
        return new Object[]{new CtanTest(value, badValue)};
    }
}
