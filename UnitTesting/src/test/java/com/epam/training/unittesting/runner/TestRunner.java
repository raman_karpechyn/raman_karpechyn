package com.epam.training.unittesting.runner;

import listeners.TestListener;
import org.testng.ITestNGListener;
import org.testng.TestNG;
import org.testng.xml.XmlSuite;

import java.util.Arrays;
import java.util.List;

public class TestRunner {
    public static void main(String[] args) {
        List<String> files = Arrays.asList("./src/test/resources/suites/ArithmeticsSuite.xml",
                "./src/test/resources/suites/FunctionsSuite.xml",
                "./src/test/resources/suites/TrigonometrysSuite.xml");

        TestNG testNG = new TestNG();
        testNG.addListener((ITestNGListener) new TestListener());
        testNG.setTestSuites(files);
        testNG.setParallel(XmlSuite.ParallelMode.TESTS);
        testNG.run();
    }
}
