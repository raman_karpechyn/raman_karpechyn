package com.epam.training.unittesting.functions;

import com.epam.training.unittesting.CalculatorTest;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class IsNegativeTest extends CalculatorTest {
    @Test
    @Parameters("positive")
    public void testIsNegativeForPositive(long pValue) {
        Assert.assertFalse(calculator.isNegative(pValue));
    }

    @Test
    @Parameters("negative")
    public void testIsNegativeForNegative(long nValue) {
        Assert.assertTrue(calculator.isNegative(nValue));
    }

    @Test
    public void testIsNegativeForZero() {
        Assert.assertFalse(calculator.isNegative(0L));
    }
}
