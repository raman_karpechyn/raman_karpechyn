package com.epam.training.unittesting.trigonometrys;

import com.epam.training.unittesting.CalculatorTest;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SinTest extends CalculatorTest {
    private double value;

    public SinTest(double value) {
        this.value = value;
    }

    @Test
    public void testSin() {
        double expected = Math.sin(value);
        double actual = calculator.sin(value);
        Assert.assertEquals(actual, expected);
    }
}
