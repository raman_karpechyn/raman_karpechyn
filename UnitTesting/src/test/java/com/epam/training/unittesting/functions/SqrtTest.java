package com.epam.training.unittesting.functions;

import com.epam.training.unittesting.CalculatorTest;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class SqrtTest extends CalculatorTest {
    @Test
    @Parameters("positiveDouble")
    public void testSqrtForPositive(double pValue) {
        double expected = Math.sqrt(pValue);
        double actual = calculator.sqrt(pValue);
        Assert.assertEquals(actual, expected);
    }

    @Test(expectedExceptions = NumberFormatException.class)
    @Parameters("negativeDouble")
    public void testSqrtForNegative(double nValue) {
        double actual = calculator.sqrt(nValue);
    }

    @Test(dependsOnMethods = "testSqrtForPositive")
    public void testSqrtForZero() {
        testSqrtForPositive(0d);
    }
}
