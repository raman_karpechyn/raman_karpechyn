package com.epam.training.unittesting.arithmetics;

import com.epam.training.unittesting.CalculatorTest;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SubTest extends CalculatorTest {
    @Test(dataProvider = "Data for long")
    public void testSubForLong(long one, long two) {
        long expected = one - two;
        long actual = calculator.sub(one, two);
        Assert.assertEquals(actual, expected);
    }

    @Test(dataProvider = "Data for double")
    public void testSubForDouble(double one, double two) {
        double expected = one - two;
        double actual = calculator.sub(one, two);
        Assert.assertEquals(actual, expected);
    }
}
