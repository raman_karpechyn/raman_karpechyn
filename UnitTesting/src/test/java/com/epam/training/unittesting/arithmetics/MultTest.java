package com.epam.training.unittesting.arithmetics;

import com.epam.training.unittesting.CalculatorTest;
import org.testng.Assert;
import org.testng.annotations.Test;

public class MultTest extends CalculatorTest {
    @Test(dataProvider = "Data for long")
    public void testMultForLong(long one, long two) {
        long expected = one * two;
        long actual = calculator.mult(one, two);
        Assert.assertEquals(actual, expected);
    }

    @Test(dataProvider = "Data for double")
    public void testMultForDouble(double one, double two) {
        double expected = one * two;
        double actual = calculator.mult(one, two);
        Assert.assertEquals(actual, expected);
    }
}
