package com.epam.training.unittesting.trigonometrys;

import com.epam.training.unittesting.CalculatorTest;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CosTest extends CalculatorTest {
    private double value;

    public CosTest(double value) {
        this.value = value;
    }

    @Test
    public void testCos() {
        double expected = Math.cos(value);
        double actual = calculator.cos(value);
        Assert.assertEquals(actual, expected);
    }
}
