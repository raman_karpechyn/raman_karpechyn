package com.epam.training.unittesting.trigonometrys;

import com.epam.training.unittesting.CalculatorTest;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CtanTest extends CalculatorTest {
    private double value;
    private double badValue;

    public CtanTest(double value, double badValue) {
        this.value = value;
        this.badValue = badValue;
    }

    @Test
    public void testCtan() {
        double expected = 1d / Math.tan(value);
        double actual = calculator.ctg(value);
        Assert.assertEquals(actual, expected);
    }

    @Test(expectedExceptions = NumberFormatException.class)
    public void testCtanForBadValue() {
        double actual = calculator.ctg(badValue);
    }
}
