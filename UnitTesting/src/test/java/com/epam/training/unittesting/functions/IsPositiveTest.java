package com.epam.training.unittesting.functions;

import com.epam.training.unittesting.CalculatorTest;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class IsPositiveTest extends CalculatorTest {
    @Test
    @Parameters("positive")
    public void testIsPositiveForPositive(long pValue) {
        Assert.assertTrue(calculator.isPositive(pValue));
    }

    @Test
    @Parameters("negative")
    public void testIsPositiveForNegative(long nValue) {
        Assert.assertFalse(calculator.isPositive(nValue));
    }

    @Test
    public void testIsPositiveForZero() {
        Assert.assertFalse(calculator.isPositive(0L));
    }
}
