package com.epam.at.hw3.po;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CheckboxradioPage extends HomePage {
    private final String radioButtonsCSS = "label[for='radio-%s']";
    private final String checkBoxesCSS = "label[for='checkbox-%s']";
    private final String checkedElementsClass = "ui-checkboxradio-checked";


    public CheckboxradioPage(WebDriver driver) {
        super(driver);
    }


    private By getCSSLocator(String locator, int index) {
        return By.cssSelector(String.format(locator, index));
    }

    public CheckboxradioPage checkRadioButton(int index) {
        switchToFrame();
        driver.findElement(getCSSLocator(radioButtonsCSS, index)).click();
        switchToDefaultContent();

        return this;
    }

    public CheckboxradioPage checkCheckBox(int index) {
        switchToFrame();
        driver.findElement(getCSSLocator(checkBoxesCSS, index)).click();
        switchToDefaultContent();

        return this;
    }

    public boolean isRadioButtonChecked(int index) {
        switchToFrame();
        String classes = driver.findElement(getCSSLocator(radioButtonsCSS, index)).getAttribute("class");
        switchToDefaultContent();

        return classes.contains(checkedElementsClass);
    }

    public boolean isCheckBoxChecked(int index) {
        switchToFrame();
        String classes = driver.findElement(getCSSLocator(checkBoxesCSS, index)).getAttribute("class");
        switchToDefaultContent();

        return classes.contains(checkedElementsClass);
    }
}
