package com.epam.at.hw3.po;

import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class DraggablePage extends HomePage {
    private final By draggableElement = By.id("draggable");


    public DraggablePage(WebDriver driver) {
        super(driver);
    }


    public DraggablePage dragElement(Point draggTo) {
        switchToFrame();

        new Actions(driver).dragAndDropBy(driver.findElement(draggableElement), draggTo.getX(), draggTo.getY())
                .build()
                .perform();

        switchToDefaultContent();

        return this;
    }

    public Point getDraggableElementPosition() {
        switchToFrame();

        WebElement element = driver.findElement(draggableElement);
        Point position = new Point(
                Integer.parseInt(element.getCssValue("left").replace("px", "")),
                Integer.parseInt(element.getCssValue("top").replace("px", ""))
        );

        switchToDefaultContent();

        return position;
    }
}
