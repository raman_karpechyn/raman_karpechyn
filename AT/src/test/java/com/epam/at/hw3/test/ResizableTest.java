package com.epam.at.hw3.test;

import com.epam.at.hw3.po.HomePage;
import org.openqa.selenium.Point;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ResizableTest extends Configuration {
    @Test
    public void testDrag() {
        Point resizeTo = new Point(170, 170);
        Point actualPosition = new HomePage(driver).goToResizablePage().resizeElement(resizeTo).getResizableElementSize();
        Assert.assertEquals(actualPosition.getX(), resizeTo.getX() + 150);
        Assert.assertEquals(actualPosition.getY(), resizeTo.getY() + 150);
    }
}
