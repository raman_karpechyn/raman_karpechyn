package com.epam.at.hw3.po;

import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class ResizablePage extends HomePage {
    private final By resizableElement = By.id("resizable");
    private final By resizableArea = By.className("ui-resizable-se");


    public ResizablePage(WebDriver driver) {
        super(driver);
    }


    public ResizablePage resizeElement(Point resizeTo) {
        switchToFrame();

        new Actions(driver).dragAndDropBy(driver.findElement(resizableArea), resizeTo.getX(), resizeTo.getY())
                .build()
                .perform();

        switchToDefaultContent();

        return this;
    }

    public Point getResizableElementSize() {
        switchToFrame();

        WebElement element = driver.findElement(resizableElement);
        Point position = new Point(
                Integer.parseInt(element.getCssValue("width").replace("px", "")),
                Integer.parseInt(element.getCssValue("height").replace("px", ""))
        );

        switchToDefaultContent();

        return position;
    }
}
