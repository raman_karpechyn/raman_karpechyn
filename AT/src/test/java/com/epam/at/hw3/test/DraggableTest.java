package com.epam.at.hw3.test;

import com.epam.at.hw3.po.HomePage;
import org.openqa.selenium.Point;
import org.testng.Assert;
import org.testng.annotations.Test;

public class DraggableTest extends Configuration {
    @Test
    public void testDrag() {
        Point dragTo = new Point(170, 170);
        Point actualPosition = new HomePage(driver).goToDraggablePage().dragElement(dragTo).getDraggableElementPosition();
        Assert.assertEquals(actualPosition.getX(), dragTo.getX());
        Assert.assertEquals(actualPosition.getY(), dragTo.getY());
    }
}
