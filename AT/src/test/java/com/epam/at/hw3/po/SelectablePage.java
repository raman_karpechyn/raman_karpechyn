package com.epam.at.hw3.po;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.util.List;

public class SelectablePage extends HomePage {
    private final By selectableElements = By.className("ui-selectee");
    private final String selectedElementsClass = "ui-selected";


    public SelectablePage(WebDriver driver) {
        super(driver);
    }


    public SelectablePage selectItems(int itemA, int itemB) {
        switchToFrame();

        List<WebElement> list = driver.findElements(selectableElements);
        new Actions(driver).keyDown(Keys.CONTROL)
                .click(list.get(--itemA))
                .click(list.get(--itemB))
                .keyUp(Keys.CONTROL)
                .build()
                .perform();

        switchToDefaultContent();

        return this;
    }

    public boolean isItemSelect(int item) {
        switchToFrame();
        String classes = driver.findElements(selectableElements).get(--item).getAttribute("class");
        switchToDefaultContent();

        return classes.contains(selectedElementsClass);
    }
}
