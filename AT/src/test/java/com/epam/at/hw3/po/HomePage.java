package com.epam.at.hw3.po;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage {
    private final By draggableLink = By.cssSelector("#sidebar a[href*='draggable']");
    private final By droppableLink = By.cssSelector("#sidebar a[href*='droppable']");
    private final By resizableLink = By.cssSelector("#sidebar a[href*='resizable']");
    private final By selectableLink = By.cssSelector("#sidebar a[href*='selectable']");
    private final By checkboxradioLink = By.cssSelector("#sidebar a[href*='checkboxradio']");
    private final By selectmenuLink = By.cssSelector("#sidebar a[href*='selectmenu']");
    private final By tooltipLink = By.cssSelector("#sidebar a[href*='tooltip']");
    private final By frame = By.cssSelector(".demo-frame");

    protected final WebDriver driver;


    public HomePage(WebDriver driver) {
        this.driver = driver;
    }

    public WebDriver getDriver() {
        return driver;
    }


    public DraggablePage goToDraggablePage() {
        waitForElementVisibility(draggableLink);
        driver.findElement(draggableLink).click();
        return new DraggablePage(driver);
    }

    public DroppablePage goToDroppablePage() {
        waitForElementVisibility(droppableLink);
        driver.findElement(droppableLink).click();
        return new DroppablePage(driver);
    }

    public ResizablePage goToResizablePage() {
        waitForElementVisibility(resizableLink);
        driver.findElement(resizableLink).click();
        return new ResizablePage(driver);
    }

    public SelectablePage goToSelectablePage() {
        waitForElementVisibility(selectableLink);
        driver.findElement(selectableLink).click();
        return new SelectablePage(driver);
    }

    public CheckboxradioPage goToCheckboxradioPage() {
        waitForElementVisibility(checkboxradioLink);
        driver.findElement(checkboxradioLink).click();
        return new CheckboxradioPage(driver);
    }

    public SelectmenuPage goToSelectmenuPage() {
        waitForElementVisibility(selectmenuLink);
        driver.findElement(selectmenuLink).click();
        return new SelectmenuPage(driver);
    }

    public TooltipPage goToTooltipPage() {
        waitForElementVisibility(tooltipLink);
        driver.findElement(tooltipLink).click();
        return new TooltipPage(driver);
    }

    protected void switchToFrame() {
        waitForElementVisibility(frame);
        driver.switchTo().frame(driver.findElement(frame));
    }

    protected void switchToDefaultContent() {
        driver.switchTo().defaultContent();
    }

    protected void waitForElementVisibility(By locator) {
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(locator));
    }
}
