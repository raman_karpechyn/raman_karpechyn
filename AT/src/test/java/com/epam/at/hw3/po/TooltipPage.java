package com.epam.at.hw3.po;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

public class TooltipPage extends HomePage {
    private final By tooltipLink = By.cssSelector("a[title*='widget']");
    private final By tooltipWidget = By.cssSelector("div[role='tooltip']");
    private final By tooltipText = By.className("ui-tooltip-content");


    public TooltipPage(WebDriver driver) {
        super(driver);
    }


    public TooltipPage moveToTooltipLink() {
        switchToFrame();

        new Actions(driver).moveToElement(driver.findElement(tooltipLink))
                .build()
                .perform();

        switchToDefaultContent();

        return this;
    }

    public String getTooltipText() {
        switchToFrame();
        waitForElementVisibility(tooltipWidget);
        String text = driver.findElement(tooltipWidget).findElement(tooltipText).getText();
        switchToDefaultContent();

        return text;
    }
}
