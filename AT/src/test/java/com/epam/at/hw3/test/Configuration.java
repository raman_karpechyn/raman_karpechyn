package com.epam.at.hw3.test;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.util.concurrent.TimeUnit;

public class Configuration {
    protected static String link = "http://jqueryui.com";

    protected static final String CHROME_PATH = "src/test/resources/chromedriver.exe";
    protected static WebDriver driver;

    @BeforeClass
    public static void setUp() {
        if (driver == null) {
            System.setProperty("webdriver.chrome.driver", CHROME_PATH);
            driver = new ChromeDriver();
            driver.manage().window().maximize();
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            driver.get(link);
        }
    }

    @AfterClass
    public static void tearDown() {
        driver.quit();
        driver = null;
    }
}
