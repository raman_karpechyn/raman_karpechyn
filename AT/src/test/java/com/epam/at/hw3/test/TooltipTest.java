package com.epam.at.hw3.test;

import com.epam.at.hw3.po.HomePage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TooltipTest extends Configuration {
    @Test
    public void testTooltip() {
        String tooltipText = new HomePage(driver).goToTooltipPage().moveToTooltipLink().getTooltipText();
        Assert.assertEquals(tooltipText, "That's what this widget is");
    }
}
