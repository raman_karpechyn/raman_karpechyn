package com.epam.at.hw3.test;

import com.epam.at.hw3.po.HomePage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CheckboxradioTest extends Configuration {
    @Test
    public void testRadioButton() {
        boolean flag = new HomePage(driver).goToCheckboxradioPage().checkRadioButton(1).isRadioButtonChecked(1);
        Assert.assertTrue(flag);
    }

    @Test
    public void testCheckBox() {
        boolean flag = new HomePage(driver).goToCheckboxradioPage().checkCheckBox(1).isCheckBoxChecked(1);
        Assert.assertTrue(flag);
    }
}
