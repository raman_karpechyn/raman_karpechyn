package com.epam.at.hw3.test;

import com.epam.at.hw3.po.HomePage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class DroppableTest extends Configuration {
    @Test
    public void testDropSquare() {
        boolean flag = new HomePage(driver).goToDroppablePage().draggElement().isElementDrop();
        Assert.assertTrue(flag);
    }
}
