package com.epam.at.hw3.test;

import com.epam.at.hw3.po.HomePage;
import com.epam.at.hw3.po.SelectablePage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SelectableTest extends Configuration {
    @Test
    public void testSelect() {
        SelectablePage select = new HomePage(driver).goToSelectablePage().selectItems(1, 3);
        boolean isOneSelected = select.isItemSelect(1);
        boolean isThreeSelected = select.isItemSelect(3);
        Assert.assertTrue(isOneSelected);
        Assert.assertTrue(isThreeSelected);
    }
}
