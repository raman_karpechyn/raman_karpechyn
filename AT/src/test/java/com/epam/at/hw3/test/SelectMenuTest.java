package com.epam.at.hw3.test;

import com.epam.at.hw3.po.HomePage;
import com.epam.at.hw3.po.SelectmenuPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SelectMenuTest extends Configuration {
    @Test
    public void testSelectMenu() {
        String speed = "Fast";
        String number = "3";

        SelectmenuPage menu = new HomePage(driver).goToSelectmenuPage().selectSpeedByValue(speed).selectNumberByValue(number);
        String speedText = menu.getTextInSpeedMenu();
        String numberText = menu.getTextInNumberMenu();

        Assert.assertEquals(speedText, speed);
        Assert.assertEquals(numberText, number);
    }
}
