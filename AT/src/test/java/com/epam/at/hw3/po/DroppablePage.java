package com.epam.at.hw3.po;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

public class DroppablePage extends HomePage {
    private final By draggableElement = By.id("draggable");
    private final By droppableElement = By.id("droppable");
    private final String elementIsDroppedClass = "ui-state-highlight";


    public DroppablePage(WebDriver driver) {
        super(driver);
    }


    public DroppablePage draggElement() {
        switchToFrame();

        new Actions(driver).dragAndDrop(driver.findElement(draggableElement), driver.findElement(droppableElement))
                .build()
                .perform();

        switchToDefaultContent();

        return this;
    }

    public boolean isElementDrop() {
        switchToFrame();
        String classes = driver.findElement(droppableElement).getAttribute("class");
        switchToDefaultContent();

        return classes.contains(elementIsDroppedClass);
    }
}
