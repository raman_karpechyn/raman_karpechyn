package com.epam.at.hw3.po;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SelectmenuPage extends HomePage {
    private final By speedMenu = By.id("speed-button");
    private final By numberMenu = By.id("number-button");
    private final By text = By.className("ui-selectmenu-text");
    private final String menuSelect = "//div[starts-with(@id, 'ui-id-') and text()='%s']";


    public SelectmenuPage(WebDriver driver) {
        super(driver);
    }


    public SelectmenuPage selectSpeedByValue(String speed) {
        switchToFrame();
        driver.findElement(speedMenu).click();
        driver.findElement(getMenuSelect(speed)).click();
        switchToDefaultContent();

        return this;
    }

    public String getTextInSpeedMenu() {
        switchToFrame();
        String menuText = driver.findElement(speedMenu).findElement(text).getText();
        switchToDefaultContent();

        return menuText;
    }

    public SelectmenuPage selectNumberByValue(String number) {
        switchToFrame();
        driver.findElement(numberMenu).click();
        driver.findElement(getMenuSelect(number)).click();
        switchToDefaultContent();

        return this;
    }

    public String getTextInNumberMenu() {
        switchToFrame();
        String menuText = driver.findElement(numberMenu).findElement(text).getText();
        switchToDefaultContent();

        return menuText;
    }

    private By getMenuSelect(String id) {
        return By.xpath(String.format(menuSelect, id));
    }
}
