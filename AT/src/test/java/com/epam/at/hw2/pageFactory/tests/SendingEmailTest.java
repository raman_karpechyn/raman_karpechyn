package com.epam.at.hw2.pageFactory.tests;

import com.epam.at.hw2.pageFactory.pf.MailBoxPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SendingEmailTest extends TestConfiguration {
    @Test
    public void testSendEmail() {
        MailBoxPage mailBox = new MailBoxPage(driver).clickCreateNewMailButton()
                .enterMailTo(mailTo)
                .enterSubject(subject)
                .enterMailBody(text)
                .clickSaveMailToDraftButton()
                .clickDraftPageLink()
                .clickMailLimkBySubject(subject)
                .clickSendMailButton()
                .clickDraftPageLink();

        Assert.assertFalse(mailBox.isMailWithSubjectExist(subject));

        mailBox.clickSendPageLink();

        Assert.assertTrue(mailBox.isMailWithSubjectExist(subject));

        Assert.assertEquals(mailBox.getMailToBySubject(subject), mailTo);
        Assert.assertEquals(mailBox.getMailSubjectBySubject(subject), subject);
        Assert.assertEquals(mailBox.getMailBodyBySubject(subject), text);
    }
}
