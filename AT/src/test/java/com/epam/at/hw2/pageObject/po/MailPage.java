package com.epam.at.hw2.pageObject.po;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

public class MailPage extends MessagesPage {
    private final By mailToField = By.cssSelector("textarea[data-original-name='To']");
    private final By subjectField = By.name("Subject");
    private final By mailBodyFrame = By.cssSelector("iframe[id^='toolkit-']");
    private final By saveToDraftButton = By.xpath("//div[@data-name='saveDraft']");
    private final By savedToDraftLabel = By.cssSelector("div[data-mnemo='saveStatus']");
    private final By sendMailButton = By.xpath("//div[@data-name='send']");
    private final By sentMailLabel = By.id("b-compose__sent");

    public MailPage(WebDriver driver) {
        super(driver);
    }

    public MailPage enterMailTo(String email) {
        waitForElementVisibility(mailToField);
        driver.findElement(mailToField).sendKeys(email);
        return this;
    }

    public MailPage enterSubject(String subject) {
        waitForElementVisibility(subjectField);
        driver.findElement(subjectField).sendKeys(subject);
        return this;
    }

    public MailPage enterMailBody(String text) {
        waitForElementVisibility(mailBodyFrame);
        driver.switchTo().frame(driver.findElement(mailBodyFrame));

        driver.findElement(bodyHTMLSelector).sendKeys(Keys.chord(Keys.CONTROL, "a"));
        driver.findElement(bodyHTMLSelector).sendKeys(Keys.DELETE);
        driver.findElement(bodyHTMLSelector).sendKeys(text);

        driver.switchTo().defaultContent();
        return this;
    }

    public MailPage clickSaveMailToDraftButton() {
        waitForElementVisibility(saveToDraftButton);
        driver.findElement(saveToDraftButton).click();
        return this;
    }

    public boolean isMailSavedToDraft() {
        waitForElementVisibility(savedToDraftLabel);
        return elementIsPresent(savedToDraftLabel);
    }

    public MailBoxPage clickSendMailButton() {
        waitForElementVisibility(sendMailButton);
        driver.findElement(sendMailButton).click();
        waitForElementVisibility(sentMailLabel);
        return new MailBoxPage(driver);
    }
}
