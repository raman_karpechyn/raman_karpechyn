package com.epam.at.hw2.pageFactory.tests;

import com.epam.at.hw2.pageFactory.pf.MailBoxPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginTest extends TestConfiguration {
    @Test
    public void testLogin() {
        Assert.assertEquals(new MailBoxPage(driver).getUserEmail(), mail);
    }
}
