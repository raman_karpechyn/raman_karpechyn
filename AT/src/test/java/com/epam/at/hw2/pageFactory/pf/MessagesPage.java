package com.epam.at.hw2.pageFactory.pf;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public abstract class MessagesPage extends Page {
    @FindBy(id = "PH_user-email")
    private WebElement userEmail;

    @FindBy(css = "a[href^='/compose/?']")
    private WebElement newMailButton;

    @FindBy(css = "a[href='/messages/inbox/']")
    private WebElement inboxButton;

    @FindBy(css = "a[href='/messages/sent/']")
    private WebElement sentButton;

    @FindBy(css = "a[href='/messages/drafts/']")
    private WebElement draftsButton;

    @FindBy(id = "PH_logoutLink")
    private WebElement logoutLink;

    public MessagesPage(WebDriver driver) {
        super(driver);
    }

    public MailBoxPage clickInboxPageLink() {
        inboxButton.click();
        return new MailBoxPage(driver);
    }

    public MailBoxPage clickSendPageLink() {
        sentButton.click();
        return new MailBoxPage(driver);
    }

    public MailBoxPage clickDraftPageLink() {
        draftsButton.click();
        return new MailBoxPage(driver);
    }

    public MailPage clickCreateNewMailButton() {
        newMailButton.click();
        return new MailPage(driver);
    }

    public String getUserEmail() {
        waitForElementVisibility(By.id("PH_user-email"));
        return userEmail.getText();
    }

    public HomePage clicklogOutLink() {
        logoutLink.click();
        return new HomePage(driver);
    }
}
