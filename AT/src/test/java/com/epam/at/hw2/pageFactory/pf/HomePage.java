package com.epam.at.hw2.pageFactory.pf;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends Page {
    @FindBy(id = "mailbox:login")
    private WebElement loginField;

    @FindBy(id = "mailbox:password")
    private WebElement passwordField;

    @FindBy(id = "mailbox:submit")
    private WebElement logInButton;

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public HomePage enterLogin(String login) {
        loginField.sendKeys(login);
        return this;
    }

    public HomePage enterPassword(String password) {
        passwordField.sendKeys(password);
        return this;
    }

    public MailBoxPage clickLogInButton() {
        logInButton.click();
        return new MailBoxPage(driver);
    }
}
