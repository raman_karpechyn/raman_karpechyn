package com.epam.at.hw2.pageFactory.tests;

import com.epam.at.hw2.pageFactory.pf.MailBoxPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CreatingEmailTest extends TestConfiguration {
    @Test
    public void testCreateEmail() {
        MailBoxPage mailBox = new MailBoxPage(driver).clickCreateNewMailButton()
                .enterMailTo(mailTo)
                .enterSubject(subject)
                .enterMailBody(text)
                .clickSaveMailToDraftButton()
                .clickDraftPageLink();

        Assert.assertEquals(mailBox.getMailToBySubject(subject), mailTo);
        Assert.assertEquals(mailBox.getMailSubjectBySubject(subject), subject);
        Assert.assertEquals(mailBox.getMailBodyBySubject(subject), text);
    }
}
