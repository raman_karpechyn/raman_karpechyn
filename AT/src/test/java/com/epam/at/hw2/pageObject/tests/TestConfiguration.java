package com.epam.at.hw2.pageObject.tests;

import com.epam.at.hw2.pageObject.po.HomePage;
import com.epam.at.hw2.pageObject.po.MailBoxPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.util.concurrent.TimeUnit;

public class TestConfiguration {
    protected static String link = "https://mail.ru";
    protected static String mail = "mq329u@mail.ru";
    protected static String login = "mq329u";
    protected static String password = "HjV4fK";
    protected static String mailTo = "raman_karpechyn@outlook.com";
    protected static String subject = Long.toString(System.currentTimeMillis());
    protected static String text = "Some text.";

    protected static final String CHROME_PATH = "src/test/resources/chromedriver.exe";
    protected static WebDriver driver;

    @BeforeClass
    public static void setUp() {
        if (driver == null) {
            System.setProperty("webdriver.chrome.driver", CHROME_PATH);
            driver = new ChromeDriver();
            driver.manage().window().maximize();
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            driver.get(link);
        }
    }

    @BeforeClass(dependsOnMethods = "setUp")
    public static void logIn() {
        new HomePage(driver).enterLogin(login)
                .enterPassword(password)
                .clickLogInButton();
    }

    @AfterClass
    public static void clearMail() {
        new MailBoxPage(driver).clickInboxPageLink()
                .deleteAllMails()
                .clickDraftPageLink()
                .deleteAllMails()
                .clickDraftPageLink()
                .deleteAllMails();
    }

    @AfterClass(dependsOnMethods = "clearMail")
    public static void logOut() {
        new MailBoxPage(driver).clicklogOutLink();
    }

    @AfterClass(dependsOnMethods = "logOut")
    public static void tearDown() {
        driver.quit();
        driver = null;
    }
}
