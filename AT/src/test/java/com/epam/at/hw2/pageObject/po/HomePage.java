package com.epam.at.hw2.pageObject.po;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomePage extends Page {
    private final By loginField = By.id("mailbox:login");
    private final By passwordField = By.id("mailbox:password");
    private final By logInButton = By.id("mailbox:submit");

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public HomePage enterLogin(String login) {
        waitForElementVisibility(loginField);
        driver.findElement(loginField).sendKeys(login);
        return this;
    }

    public HomePage enterPassword(String password) {
        waitForElementVisibility(passwordField);
        driver.findElement(passwordField).sendKeys(password);
        return this;
    }

    public MailBoxPage clickLogInButton() {
        waitForElementVisibility(logInButton);
        driver.findElement(logInButton).click();
        return new MailBoxPage(driver);
    }
}
