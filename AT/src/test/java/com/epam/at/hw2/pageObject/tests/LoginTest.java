package com.epam.at.hw2.pageObject.tests;

import com.epam.at.hw2.pageObject.po.MailBoxPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginTest extends TestConfiguration {
    @Test
    public void testLogin() {
        Assert.assertEquals(new MailBoxPage(driver).getUserEmail(), mail);
    }
}
