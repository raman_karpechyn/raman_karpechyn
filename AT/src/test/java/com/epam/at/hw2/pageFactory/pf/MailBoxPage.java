package com.epam.at.hw2.pageFactory.pf;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class MailBoxPage extends MessagesPage {
    @FindBy(css = "div[class='b-datalist b-datalist_letters b-datalist_letters_to']")
    private WebElement mailsBox;

    @FindBy(xpath = "//a[@data-subject]")
    private List<WebElement> mails;

    @FindBy(css = "body")
    private WebElement bodyHTMLSelector;

    public MailBoxPage(WebDriver driver) {
        super(driver);
    }

    public MailPage clickMailLimkBySubject(String subject) {
        getMailBySubject(subject).click();
        return new MailPage(driver);
    }

    public String getMailToBySubject(String subject) {
        return getMailBySubject(subject).findElement(By.className("b-datalist__item__addr")).getText();
    }

    public String getMailSubjectBySubject(String subject) {
        return getMailBySubject(subject).findElement(By.className("b-datalist__item__subj")).getText().replaceFirst(getMailBodyBySubject(subject), "");
    }

    public String getMailBodyBySubject(String subject) {
        return getMailBySubject(subject).findElement(By.className("b-datalist__item__subj__snippet")).getText();
    }

    public MailBoxPage deleteAllMails() {
        bodyHTMLSelector.sendKeys(Keys.chord(Keys.CONTROL, "a"));
        bodyHTMLSelector.sendKeys(Keys.DELETE);
        return this;
    }

    public boolean isMailWithSubjectExist(String subject) {
        if (getMailBySubject(subject) == null) {
            return false;
        }
        return true;
    }

    private WebElement getMailBySubject(String subject) {
        for (WebElement x : mails) {
            if (x.getAttribute("data-subject").contains(subject)) {
                return x;
            }
        }
        return null;
    }
}
