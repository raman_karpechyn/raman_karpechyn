package com.epam.at.hw2.pageFactory.pf;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MailPage extends MessagesPage {
    @FindBy(css = "textarea[data-original-name='To']")
    private WebElement mailToField;

    @FindBy(name = "Subject")
    private WebElement subjectField;

    @FindBy(css = "iframe[id^='toolkit-']")
    private WebElement mailBodyFrame;

    @FindBy(xpath = "//div[@data-name='saveDraft']")
    private WebElement saveToDraftButton;

    @FindBy(xpath = "//div[@data-name='send']")
    private WebElement sendMailButton;

    public MailPage(WebDriver driver) {
        super(driver);
    }

    public MailPage enterMailTo(String email) {
        mailToField.sendKeys(email);
        return this;
    }

    public MailPage enterSubject(String subject) {
        subjectField.sendKeys(subject);
        return this;
    }

    public MailPage enterMailBody(String text) {
        driver.switchTo().frame(mailBodyFrame);

        driver.findElement(By.cssSelector("body")).sendKeys(Keys.chord(Keys.CONTROL, "a"));
        driver.findElement(By.cssSelector("body")).sendKeys(Keys.DELETE);
        driver.findElement(By.cssSelector("body")).sendKeys(text);

        driver.switchTo().defaultContent();
        return this;
    }

    public MailPage clickSaveMailToDraftButton() {
        saveToDraftButton.click();
        waitForElementVisibility(By.cssSelector("div[data-mnemo='saveStatus']"));
        return this;
    }

    public MailBoxPage clickSendMailButton() {
        waitForElementVisibility(By.xpath("//div[@data-name='send']"));
        sendMailButton.click();
        waitForElementVisibility(By.id("b-compose__sent"));
        return new MailBoxPage(driver);
    }
}
