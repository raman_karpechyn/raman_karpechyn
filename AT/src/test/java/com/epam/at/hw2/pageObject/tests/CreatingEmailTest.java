package com.epam.at.hw2.pageObject.tests;

import com.epam.at.hw2.pageObject.po.MailBoxPage;
import com.epam.at.hw2.pageObject.po.MailPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CreatingEmailTest extends TestConfiguration {
    @Test
    public void testCreateEmail() {
        MailPage mail = new MailBoxPage(driver).clickCreateNewMailButton()
                .enterMailTo(mailTo)
                .enterSubject(subject)
                .enterMailBody(text)
                .clickSaveMailToDraftButton();

        Assert.assertTrue(mail.isMailSavedToDraft());

        MailBoxPage mailBox = mail.clickDraftPageLink();

        Assert.assertEquals(mailBox.getMailToBySubject(subject), mailTo);
        Assert.assertEquals(mailBox.getMailSubjectBySubject(subject), subject);
        Assert.assertEquals(mailBox.getMailBodyBySubject(subject), text);
    }
}
