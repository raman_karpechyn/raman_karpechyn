package com.epam.at.hw2.pageObject.po;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class MailBoxPage extends MessagesPage {
    private final By mailsBox = By.cssSelector("div[class='b-datalist b-datalist_letters b-datalist_letters_to']");
    private final By mailtoLabel = By.className("b-datalist__item__addr");
    private final By subjectLabel = By.className("b-datalist__item__subj");
    private final By mailBodyLabel = By.className("b-datalist__item__subj__snippet");
    private final String mailWithSubjectSelector = "//a[@data-subject='%s']";

    public MailBoxPage(WebDriver driver) {
        super(driver);
    }

    public MailPage clickMailLimkBySubject(String subject) {
        By mailInfoXPath = getMassageLocatorBySubject(subject);
        waitForElementVisibility(mailInfoXPath);
        driver.findElement(mailInfoXPath).click();
        return new MailPage(driver);
    }

    public String getMailToBySubject(String subject) {
        return getMailBySubject(subject).findElement(mailtoLabel).getText();
    }

    public String getMailSubjectBySubject(String subject) {
        return getMailBySubject(subject).findElement(subjectLabel).getText().replaceFirst(getMailBodyBySubject(subject), "");
    }

    public String getMailBodyBySubject(String subject) {
        return getMailBySubject(subject).findElement(mailBodyLabel).getText();
    }

    public MailBoxPage deleteAllMails() {
        waitForElementVisibility(bodyHTMLSelector);
        driver.findElement(bodyHTMLSelector).sendKeys(Keys.chord(Keys.CONTROL, "a"));
        driver.findElement(bodyHTMLSelector).sendKeys(Keys.DELETE);
        return this;
    }

    public boolean isMailWithSubjectExist(String subject) {
        waitForElementVisibility(mailsBox);
        return elementIsPresent(getMassageLocatorBySubject(subject));
    }

    private WebElement getMailBySubject(String subject) {
        waitForElementVisibility(mailtoLabel);
        return driver.findElement(getMassageLocatorBySubject(subject));
    }

    private By getMassageLocatorBySubject(String subject) {
        return By.xpath(String.format(mailWithSubjectSelector, subject));
    }
}
