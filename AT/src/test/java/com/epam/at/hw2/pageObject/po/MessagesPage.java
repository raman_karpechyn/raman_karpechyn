package com.epam.at.hw2.pageObject.po;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public abstract class MessagesPage extends Page {
    private final By userEmail = By.id("PH_user-email");
    private final By newMailButton = By.cssSelector("a[href^='/compose/?']");
    private final By inboxButton = By.cssSelector("a[href='/messages/inbox/']");
    private final By sentButton = By.cssSelector("a[href='/messages/sent/']");
    private final By draftsButton = By.cssSelector("a[href='/messages/drafts/']");
    private final By logoutLink = By.id("PH_logoutLink");

    protected final By bodyHTMLSelector = By.cssSelector("body");

    public MessagesPage(WebDriver driver) {
        super(driver);
    }

    public MailBoxPage clickInboxPageLink() {
        waitForElementVisibility(inboxButton);
        driver.findElement(inboxButton).click();
        return new MailBoxPage(driver);
    }

    public MailBoxPage clickSendPageLink() {
        waitForElementVisibility(sentButton);
        driver.findElement(sentButton).click();
        return new MailBoxPage(driver);
    }

    public MailBoxPage clickDraftPageLink() {
        waitForElementVisibility(draftsButton);
        driver.findElement(draftsButton).click();
        return new MailBoxPage(driver);
    }

    public MailPage clickCreateNewMailButton() {
        waitForElementVisibility(newMailButton);
        driver.findElement(newMailButton).click();
        return new MailPage(driver);
    }

    public String getUserEmail() {
        waitForElementVisibility(userEmail);
        return driver.findElement(userEmail).getText();
    }

    public HomePage clicklogOutLink() {
        driver.findElement(logoutLink).click();
        return new HomePage(driver);
    }
}
