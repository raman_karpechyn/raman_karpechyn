package com.epam.at.hw7;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.*;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class TestsWithHttpClient {
    private static String url = "http://jsonplaceholder.typicode.com/albums";
    private static int id = 42;

    @Test
    public void testHTTPResponseStatusCode() throws IOException {
        Assert.assertEquals(HttpClients.createDefault().execute(new HttpGet(url)).getStatusLine().getStatusCode(), 200);
    }

    @Test
    public void testHTTPResponseHeader() throws IOException {
        CloseableHttpResponse response = HttpClients.createDefault().execute(new HttpGet(url));

        Assert.assertTrue(response.containsHeader("content-type"));
        Assert.assertEquals(response.getHeaders("content-type")[0].getValue(), "application/json; charset=utf-8");
    }

    @Test
    public void testHTTPResponseBodySize() throws IOException {
        Assert.assertEquals(new JSONArray(getResponseBody(HttpClients.createDefault().execute(new HttpGet(url)))).length(), 100);
    }

    @Test
    public void testGettingAlbumById() throws IOException {
        JSONObject body = new JSONObject(getResponseBody(HttpClients.createDefault().execute(new HttpGet(url + "/" + id))));

        Assert.assertTrue(body.has("userId"));
        Assert.assertTrue(body.has("id"));
        Assert.assertTrue(body.has("title"));
    }

    @Test
    public void testCreatingAlbum() throws IOException {
        HttpPost httpPost = new HttpPost(url);
        httpPost.setEntity(new StringEntity("{\n" +
                                            "    \"userId\": 1,\n" +
                                            "    \"title\": \"test album\"\n" +
                                            "  }"));
        httpPost.setHeader("Content-type", "application/json");

        CloseableHttpResponse response = HttpClients.createDefault().execute(httpPost);
        String responseBody = getResponseBody(response);

        Assert.assertEquals(response.getStatusLine().getStatusCode(), 201);
        Assert.assertFalse(responseBody.isEmpty());

        System.out.println(new JSONObject(responseBody).get("id"));
    }

    @Test
    public void testUpdatingAlbum() throws IOException {
        HttpPut httpPut = new HttpPut(url + "/" + id);
        httpPut.setEntity(new StringEntity("{\n" +
                                           "    \"userId\": 1,\n" +
                                           "    \"title\": \"test album update\"\n" +
                                           "  }"));
        httpPut.setHeader("Content-type", "application/json");

        CloseableHttpResponse response = HttpClients.createDefault().execute(httpPut);

        Assert.assertEquals(response.getStatusLine().getStatusCode(), 200);
        Assert.assertEquals(new JSONObject(getResponseBody(response)).get("id"), id);
    }

    @Test()
    public void testDeletingAlbum() throws IOException {
        HttpDelete httpDelete = new HttpDelete(url + "/" + id);
        httpDelete.setHeader("Accept", "application/json");

        Assert.assertEquals(HttpClients.createDefault().execute(httpDelete).getStatusLine().getStatusCode(), 200);
    }

    private static String getResponseBody(HttpResponse response) throws IOException {
        BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        StringBuilder result = new StringBuilder();
        String line;
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }

        return result.toString();
    }
}
