package com.epam.at.hw7;

import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Headers;
import com.jayway.restassured.response.ResponseBody;
import org.testng.Assert;
import org.testng.annotations.Test;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class TestsWithRestAssured {
    private static String url = "http://jsonplaceholder.typicode.com/albums";
    private static int id = 42;

    @Test
    public void testHTTPResponseStatusCode() {
        int code = given().when()
                          .get(url)
                          .then()
                          .extract().statusCode();

        Assert.assertEquals(code, 200);
    }

    @Test
    public void testHTTPResponseHeader() {
        Headers headers = given().when()
                                 .get(url)
                                 .then().statusCode(200)
                                 .extract().headers();

        Assert.assertTrue(headers.hasHeaderWithName("content-type"));
        Assert.assertEquals(headers.get("content-type").getValue(), "application/json; charset=utf-8");
    }

    @Test
    public void testHTTPResponseBodySize() {
        ResponseBody body = given().when()
                                   .get(url)
                                   .then().statusCode(200)
                                   .extract()
                                   .response().getBody();

        Assert.assertEquals(body.path("list.size()"), 100);
    }

    @Test
    public void testGettingAlbumById() {
        given().when()
               .get(url + "/" + id)
               .then().statusCode(200)
               .assertThat().body("size", nullValue())
               .assertThat().body("$", hasKey("userId"))
               .assertThat().body("$", hasKey("id"))
               .assertThat().body("$", hasKey("title"));
    }

    @Test
    public void testCreatingAlbum() {
        int id = given().body("{\n" +
                              "    \"userId\": 1,\n" +
                              "    \"title\": \"test album\"\n" +
                              "  }")
                        .contentType(ContentType.JSON)
                        .when()
                        .post(url)
                        .then()
                        .assertThat().statusCode(201)
                        .assertThat().body("isEmpty()", is(false))
                        .extract().path("id");

        System.out.println(id);
    }

    @Test
    public void testUpdatingAlbum() {
        given().body("{\n" +
                     "    \"userId\": 1,\n" +
                     "    \"title\": \"test album update\"\n" +
                     "  }")
               .when()
               .put(url + "/" + id)
               .then()
               .assertThat().statusCode(200)
               .assertThat().body("id", is(id));
    }

    @Test()
    public void testDeletingAlbum() {
        given().when()
               .delete(url + "/" + id)
               .then()
               .assertThat().statusCode(200);
    }
}
