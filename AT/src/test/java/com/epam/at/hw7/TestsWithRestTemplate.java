package com.epam.at.hw7;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestsWithRestTemplate {
    private static String url = "http://jsonplaceholder.typicode.com/albums";
    private static int id = 42;

    @Test
    public void testHTTPResponseStatusCode() {
        Assert.assertEquals(new RestTemplate().getForEntity(url, String.class).getStatusCode(), HttpStatus.OK);
    }

    @Test
    public void testHTTPResponseHeader() {
        HttpHeaders headers = new RestTemplate().getForEntity(url, String.class)
                                                .getHeaders();

        Assert.assertTrue(headers.containsKey("content-type"));
        Assert.assertEquals(headers.get("content-type").get(0), "application/json; charset=utf-8");
    }

    @Test
    public void testHTTPResponseBodySize() {
        JSONArray body = new JSONArray(new RestTemplate().getForEntity(url, String.class)
                                                         .getBody());

        Assert.assertEquals(body.length(), 100);
    }

    @Test
    public void testGettingAlbumById() {
        JSONObject body = new JSONObject(new RestTemplate().getForEntity(url + "/" + id, String.class)
                                                           .getBody());

        Assert.assertTrue(body.has("userId"));
        Assert.assertTrue(body.has("id"));
        Assert.assertTrue(body.has("title"));
    }

    @Test
    public void testCreatingAlbum() {
        JSONObject request = new JSONObject().put("userId", "1")
                                             .put("title", "test album");

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> entity = new HttpEntity<String>(request.toString(), headers);

        ResponseEntity<String> response = new RestTemplate().postForEntity(url, entity, String.class);

        Assert.assertEquals(response.getStatusCode(), HttpStatus.CREATED);
        Assert.assertFalse(response.getBody().isEmpty());

        System.out.println(new JSONObject(response.getBody()).get("id"));
    }

    @Test
    public void testUpdatingAlbum() {
        JSONObject request = new JSONObject().put("userId", "1")
                                             .put("title", "test album update");

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> entity = new HttpEntity<String>(request.toString(), headers);

        ResponseEntity<String> response = new RestTemplate().exchange(url + "/" + id, HttpMethod.PUT, entity, String.class);

        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
        Assert.assertEquals(new JSONObject(response.getBody()).get("id"), id);
    }

    @Test()
    public void testDeletingAlbum() {
        Assert.assertEquals(new RestTemplate().exchange(url + "/" + id, HttpMethod.DELETE, null, String.class).getStatusCode(), HttpStatus.OK);
    }
}
