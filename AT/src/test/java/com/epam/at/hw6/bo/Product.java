package com.epam.at.hw6.bo;

public class Product {
    private String name;
    private String price;
    private String seller;

    public Product(String name, String price, String seller) {
        this.name = name;
        this.price = price;
        this.seller = seller;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        Product product = (Product) obj;
        return name.equals(product.name)
                && price.equals(product.price)
                && seller.equals(product.seller);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((price == null) ? 0 : price.hashCode());
        result = prime * result + ((seller == null) ? 0 : seller.hashCode());
        return result;
    }
}
