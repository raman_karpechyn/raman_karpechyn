package com.epam.at.hw6.po;

import org.openqa.selenium.By;

public class SearchResultPage extends Page {
    private final String productCssLocator = "ul#ListViewInner>*:nth-of-type(%1$s) h3,li#srp-river-results-listing%1$s h3";

    public String getProductTitleById(int id) {
        return getText(getProductLocatorById(id));
    }

    public ProductPage clickProductLinkById(int id) {
        click(getProductLocatorById(id));
        return new ProductPage();
    }

    private By getProductLocatorById(int id) {
        return By.cssSelector(String.format(productCssLocator, id));
    }
}
