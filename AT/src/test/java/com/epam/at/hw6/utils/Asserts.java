package com.epam.at.hw6.utils;

import com.epam.at.hw6.bo.Product;

public class Asserts {
    public static boolean assertProductEquals(Product productOne, Product productTwo) {
        return productOne.equals(productTwo);
    }
}
