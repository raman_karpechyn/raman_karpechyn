package com.epam.at.hw6.po;

import org.openqa.selenium.By;

public class ProductPage extends Page {
    private final By productTitle = By.id("itemTitle");
    private final By productPrice = By.id("prcIsum");
    private final By productSeller = By.cssSelector("a#mbgLink span");
    private final By addToCartButton = By.id("isCartBtn_btn");

    public String getProductTitle() {
        return getText(productTitle);
    }

    public String getProductPrice() {
        return getText(productPrice);
    }

    public String getProductSeller() {
        return getText(productSeller);
    }

    public CartPage clickAddToCartButton() {
        click(addToCartButton);
        return new CartPage();
    }
}
