package com.epam.at.hw6.steps;

import com.epam.at.hw6.bo.Product;
import com.epam.at.hw6.services.CartService;
import com.epam.at.hw6.services.ProductService;
import com.epam.at.hw6.services.SearchService;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.testng.Assert;

public class EbayProductSearchDefinition {
    private Product product;

    @Given("^I open eBay Page$")
    public void iOpenEbayPage() {
        new SearchService().open();
    }

    @When("^I search the product \"([^\"]*)\"$")
    public void iSearchTheProduct(String query) {
        new SearchService().search(query);
    }

    @Then("^The \"([^\"]*)\" product on the Search Result page should contains query \"([^\"]*)\"$")
    public void theProductOnTheSearchResultPageShouldContainsQuery(int id, String expectedPhrase) {
        Assert.assertTrue(new SearchService().isProductMatch(id, expectedPhrase));
    }

    @And("^I click on the \"([^\"]*)\" product$")
    public void iClickOnTheProduct(int id) {
        new SearchService().clickOnProductById(id);
    }

    @And("^I save selected product$")
    public void iSaveSelectedProduct() {
        product = new ProductService().getProduct();
    }

    @And("^I click on Add to Cart button$")
    public void iClickOnAddToCartButton() {
        new ProductService().addToCart();
    }

    @Then("^Saved product should be equal with product in the cart$")
    public void savedProductShouldBeEqualWithProductInTheCart() {
        Assert.assertTrue(new CartService().isProductMatch(product));
    }
}
