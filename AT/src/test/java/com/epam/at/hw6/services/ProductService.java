package com.epam.at.hw6.services;

import com.epam.at.hw6.bo.Product;
import com.epam.at.hw6.po.ProductPage;

public class ProductService {
    public Product getProduct() {
        ProductPage page = new ProductPage();
        return new Product(page.getProductTitle(), page.getProductPrice(), page.getProductSeller());
    }

    public CartService addToCart() {
        new ProductPage().clickAddToCartButton();
        return new CartService();
    }
}
