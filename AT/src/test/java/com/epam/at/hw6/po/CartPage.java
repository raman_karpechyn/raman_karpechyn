package com.epam.at.hw6.po;

import org.openqa.selenium.By;

public class CartPage extends Page {
    private final By productTitle = By.cssSelector("a[data-test-id='cart-item-link'] span span");
    private final By productPrice = By.cssSelector("div.item-price span span");
    private final By productSeller = By.cssSelector("a[href*='usr'] span");

    public String getProductTitle() {
        return getText(productTitle);
    }

    public String getProductPrice() {
        return getText(productPrice);
    }

    public String getProductSeller() {
        return getText(productSeller);
    }
}
