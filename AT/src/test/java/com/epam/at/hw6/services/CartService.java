package com.epam.at.hw6.services;

import com.epam.at.hw6.utils.Asserts;
import com.epam.at.hw6.bo.Product;
import com.epam.at.hw6.po.CartPage;

public class CartService {
    public Product getProduct() {
        CartPage page = new CartPage();
        return new Product(page.getProductTitle(), page.getProductPrice(), page.getProductSeller());
    }

    public boolean isProductMatch(Product product) {
        return Asserts.assertProductEquals(getProduct(), product);
    }
}
