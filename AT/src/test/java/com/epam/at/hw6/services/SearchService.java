package com.epam.at.hw6.services;


import com.epam.at.hw6.po.HomePage;
import com.epam.at.hw6.po.SearchResultPage;

public class SearchService {
    public SearchService open() {
        new HomePage().open();
        return this;
    }

    public SearchService search(String query) {
        new HomePage().enterSearchQuery(query)
                      .clickSearchButton();
        return this;
    }

    public ProductService clickOnProductById(int id) {
        new SearchResultPage().clickProductLinkById(id);
        return new ProductService();
    }

    public boolean isProductMatch(int id, String query) {
        return new SearchResultPage().getProductTitleById(id)
                                     .toLowerCase()
                                     .contains(query.toLowerCase());
    }
}
