package com.epam.at.hw6.po;

import org.openqa.selenium.By;

public class HomePage extends Page {
    private final By searchBar = By.cssSelector("div#gh-ac-box2 input");
    private final By searchButton = By.id("gh-btn");

    private final String url = "https://www.ebay.com/";

    public HomePage open() {
        driver.get(url);
        return this;
    }

    public HomePage enterSearchQuery(String query) {
        enterText(searchBar, query);
        return this;
    }

    public SearchResultPage clickSearchButton() {
        click(searchButton);
        return new SearchResultPage();
    }
}
