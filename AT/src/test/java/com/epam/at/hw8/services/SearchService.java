package com.epam.at.hw8.services;

import com.epam.at.hw8.po.HomePage;
import com.epam.at.hw8.po.SearchResultPage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class SearchService {
    private final Logger logger = LogManager.getLogger(this.getClass());

    public SearchService open() {
        new HomePage().open()
                      .highlightSearchBar();
        return this;
    }

    public SearchService search(String query) {
        logger.info("Searching \"" + query + "\" query");
        new HomePage().enterSearchQuery(query)
                      .clickSearchButton()
                      .highlightProductById(1);
        return this;
    }

    public ProductService clickOnProductById(int id) {
        new SearchResultPage().clickProductLinkById(id)
                              .highlightProductData();
        return new ProductService();
    }

    public boolean isProductMatch(int id, String query) {
        logger.info("Matching product with id \"" + id + "\" to \"" + query + "\" query");
        return new SearchResultPage().getProductTitleById(id)
                                     .toLowerCase()
                                     .contains(query.toLowerCase());
    }
}
