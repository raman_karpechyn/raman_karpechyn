package com.epam.at.hw8.services;

import com.epam.at.hw8.bo.Product;
import com.epam.at.hw8.po.ProductPage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class ProductService {
    private final Logger logger = LogManager.getLogger(this.getClass());

    public Product getProduct() {
        ProductPage page = new ProductPage();
        return new Product(page.getProductTitle(), page.getProductPrice(), page.getProductSeller());
    }

    public CartService addToCart() {
        logger.info("Add product to cart");
        new ProductPage().clickAddToCartButton()
                         .highlightProductData();
        return new CartService();
    }
}
