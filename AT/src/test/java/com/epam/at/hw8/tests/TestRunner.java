package com.epam.at.hw8.tests;

import com.epam.at.hw8.impl.WebDriverSingleton;
import com.epam.at.hw8.listeners.TestListener;
import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;

import java.util.concurrent.TimeUnit;

@CucumberOptions(
        features = "src/test/resources/hw8.features",
        glue = "com.epam.at.hw8.steps",
        strict = true,
        plugin = {
                "pretty", "json:target/Cucumber.json",
                "html:target/cucumber-html-report"
        }
)

@Listeners(TestListener.class)
public class TestRunner extends AbstractTestNGCucumberTests {
    @BeforeClass
    public static void setUp() {
        WebDriverSingleton.getInstance()
                          .manage()
                          .window()
                          .maximize();
        WebDriverSingleton.getInstance()
                          .manage()
                          .timeouts()
                          .implicitlyWait(10, TimeUnit.SECONDS);
    }

    @AfterClass
    public static void tearDown() {
        WebDriverSingleton.getInstance()
                          .quit();
    }
}
