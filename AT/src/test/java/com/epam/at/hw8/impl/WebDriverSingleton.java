package com.epam.at.hw8.impl;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class WebDriverSingleton {
    private static WebDriver driver;

    private static Logger logger = LogManager.getLogger(WebDriverSingleton.class);

    private WebDriverSingleton() {
    }

    public static WebDriver getInstance() {
        if (driver == null) {
            WebDriverManager.chromedriver()
                            .setup();
            driver = new ChromeDriver();
            logger.info("WebDriver initialized");
        }
        return driver;
    }
}
