package com.epam.at.hw8.po;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;

public class CartPage extends Page {
    private final By productTitle = By.cssSelector("a[data-test-id='cart-item-link'] span span");
    private final By productPrice = By.cssSelector("div.item-price span span");
    private final By productSeller = By.cssSelector("a[href*='usr'] span");

    private final Logger logger = LogManager.getLogger(this.getClass());

    public String getProductTitle() {
        logger.debug("Getting product title");
        return getText(productTitle);
    }

    public String getProductPrice() {
        logger.debug("Getting product price");
        return getText(productPrice);
    }

    public String getProductSeller() {
        logger.debug("Getting product seller");
        return getText(productSeller);
    }

    public CartPage highlightProductData() {
        highlightElement(productTitle);
        highlightElement(productPrice);
        highlightElement(productSeller);
        return this;
    }
}
