package com.epam.at.hw8.po;

import com.epam.at.hw8.impl.WebDriverSingleton;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class Page {
    protected final WebDriver driver = WebDriverSingleton.getInstance();

    private final Logger logger = LogManager.getLogger(this.getClass());

    protected void click(By locator) {
        waitForElementVisibility(locator);
        driver.findElement(locator).click();
        logger.debug("Element is clicked");
    }

    protected void enterText(By locator, String text) {
        waitForElementVisibility(locator);
        driver.findElement(locator).sendKeys(text);
        logger.debug("\"" + text + "\" string is typed to element");
    }

    protected String getText(By locator) {
        waitForElementVisibility(locator);
        String elementText = driver.findElement(locator).getText();
        logger.debug("\"" + elementText + "\" string is got from element");
        return elementText;
    }

    protected void highlightElement(By locator) {
        waitForElementVisibility(locator);
        if (driver instanceof JavascriptExecutor) {
            ((JavascriptExecutor) driver).executeScript("arguments[0].style.border='3px solid orange'", driver.findElement(locator));
        }
    }

    protected void waitForElementVisibility(By locator) {
        try {
            new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(locator));
        } catch (WebDriverException ex) {
            logger.error("WebDriver error");
            logger.error(ex.getMessage());
        }
    }
}
