package com.epam.at.hw8.listeners;

import com.epam.at.hw8.utils.CaptureScreenShot;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class TestListener implements ITestListener {
    private Logger logger = LogManager.getLogger(this.getClass());

    @Override
    public void onTestStart(ITestResult iTestResult) {

    }

    @Override
    public void onTestSuccess(ITestResult iTestResult) {
        logger.info("Test \"" + iTestResult.getMethod().getMethodName() + "\" was passed");
        CaptureScreenShot.saveScreenShot("OnSuccess");
    }

    @Override
    public void onTestFailure(ITestResult iTestResult) {
        logger.error("Test \"" + iTestResult.getMethod().getMethodName() + "\" was failed");
        CaptureScreenShot.saveScreenShot("OnFail");
    }

    @Override
    public void onTestSkipped(ITestResult iTestResult) {

    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {

    }

    @Override
    public void onStart(ITestContext iTestContext) {

    }

    @Override
    public void onFinish(ITestContext iTestContext) {

    }
}
