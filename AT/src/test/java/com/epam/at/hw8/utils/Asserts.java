package com.epam.at.hw8.utils;

import com.epam.at.hw8.bo.Product;

public class Asserts {
    public static boolean assertProductEquals(Product productOne, Product productTwo) {
        return productOne.equals(productTwo);
    }
}
