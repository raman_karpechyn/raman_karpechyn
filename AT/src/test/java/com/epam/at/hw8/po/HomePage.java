package com.epam.at.hw8.po;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriverException;

public class HomePage extends Page {
    private final By searchBar = By.cssSelector("div#gh-ac-box2 input");
    private final By searchButton = By.id("gh-btn");

    private final String url = "https://www.ebay.com/";

    private final Logger logger = LogManager.getLogger(this.getClass());

    public HomePage open() {
        logger.info("Try to open " + url);
        try {
            driver.get(url);
            logger.debug(url + " is open");
        } catch (WebDriverException ex) {
            logger.error(url + " doesn't open");
            logger.error(ex.getMessage());
        }
        return this;
    }

    public HomePage enterSearchQuery(String query) {
        logger.debug("Enter search query \"" + query + "\"");
        enterText(searchBar, query);
        return this;
    }

    public SearchResultPage clickSearchButton() {
        logger.debug("Click search button");
        click(searchButton);
        logger.info("Move to search result page");
        return new SearchResultPage();
    }

    public HomePage highlightSearchBar() {
        highlightElement(searchBar);
        return this;
    }
}
