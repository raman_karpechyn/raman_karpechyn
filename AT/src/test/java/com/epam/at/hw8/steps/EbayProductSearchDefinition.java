package com.epam.at.hw8.steps;

import com.epam.at.hw8.bo.Product;
import com.epam.at.hw8.services.CartService;
import com.epam.at.hw8.services.ProductService;
import com.epam.at.hw8.services.SearchService;
import com.epam.at.hw8.utils.CaptureScreenShot;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.testng.Assert;

public class EbayProductSearchDefinition {
    private Product product;

    @Given("^I open eBay Page$")
    public void iOpenEbayPage() {
        new SearchService().open();
        CaptureScreenShot.saveScreenShot("");
    }

    @When("^I search the product \"([^\"]*)\"$")
    public void iSearchTheProduct(String query) {
        new SearchService().search(query);
        CaptureScreenShot.saveScreenShot("");
    }

    @Then("^The \"([^\"]*)\" product on the Search Result page should contains query \"([^\"]*)\"$")
    public void theProductOnTheSearchResultPageShouldContainsQuery(int id, String expectedPhrase) {
        Assert.assertTrue(new SearchService().isProductMatch(id, expectedPhrase));
        CaptureScreenShot.saveScreenShot("");
    }

    @And("^I click on the \"([^\"]*)\" product$")
    public void iClickOnTheProduct(int id) {
        new SearchService().clickOnProductById(id);
    }

    @And("^I save selected product$")
    public void iSaveSelectedProduct() {
        product = new ProductService().getProduct();
    }

    @And("^I click on Add to Cart button$")
    public void iClickOnAddToCartButton() {
        new ProductService().addToCart();
    }

    @Then("^Saved product should be equal with product in the cart$")
    public void savedProductShouldBeEqualWithProductInTheCart() {
        Assert.assertTrue(new CartService().isProductMatch(product));
        CaptureScreenShot.saveScreenShot("");
    }
}
