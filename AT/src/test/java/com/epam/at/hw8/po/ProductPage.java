package com.epam.at.hw8.po;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;

public class ProductPage extends Page {
    private final By productTitle = By.id("itemTitle");
    private final By productPrice = By.id("prcIsum");
    private final By productSeller = By.cssSelector("a#mbgLink span");
    private final By addToCartButton = By.id("isCartBtn_btn");

    private final Logger logger = LogManager.getLogger(this.getClass());

    public String getProductTitle() {
        logger.debug("Getting product title");
        return getText(productTitle);
    }

    public String getProductPrice() {
        logger.debug("Getting product price");
        return getText(productPrice);
    }

    public String getProductSeller() {
        logger.debug("Getting product seller");
        return getText(productSeller);
    }

    public CartPage clickAddToCartButton() {
        logger.debug("Click \"Add to cart\" button");
        click(addToCartButton);
        logger.info("Move to cart page");
        return new CartPage();
    }

    public ProductPage highlightProductData() {
        highlightElement(productTitle);
        highlightElement(productPrice);
        highlightElement(productSeller);
        return this;
    }
}
