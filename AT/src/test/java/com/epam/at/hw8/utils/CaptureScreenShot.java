package com.epam.at.hw8.utils;

import com.epam.at.hw8.impl.WebDriverSingleton;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CaptureScreenShot {
    public static void saveScreenShot(String name) {
        File scrFile = ((TakesScreenshot) WebDriverSingleton.getInstance()).getScreenshotAs(OutputType.FILE);

        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd--HH-mm-ss-SS");
        String formattedDate = sdf.format(date);
        String fileName = (name.isEmpty() ? "screenshot-" : name) + formattedDate;

        try {
            FileUtils.copyFile(scrFile, new File(String.format("target/screenshots/%s.png", fileName)));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
