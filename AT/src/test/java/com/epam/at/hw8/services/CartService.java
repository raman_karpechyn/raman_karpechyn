package com.epam.at.hw8.services;

import com.epam.at.hw8.utils.Asserts;
import com.epam.at.hw8.bo.Product;
import com.epam.at.hw8.po.CartPage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class CartService {
    private final Logger logger = LogManager.getLogger(this.getClass());

    public Product getProduct() {
        CartPage page = new CartPage();
        return new Product(page.getProductTitle(), page.getProductPrice(), page.getProductSeller());
    }

    public boolean isProductMatch(Product product) {
        logger.info("Matching product in the cart with saved product");
        return Asserts.assertProductEquals(getProduct(), product);
    }
}
