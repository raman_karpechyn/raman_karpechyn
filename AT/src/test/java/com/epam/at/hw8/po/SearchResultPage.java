package com.epam.at.hw8.po;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;

public class SearchResultPage extends Page {
    private final String productCssLocator = "ul#ListViewInner>*:nth-of-type(%1$s) h3,li#srp-river-results-listing%1$s h3";

    private final Logger logger = LogManager.getLogger(this.getClass());

    public String getProductTitleById(int id) {
        logger.debug("Getting product title with id \"" + id + "\"");
        return getText(getProductLocatorById(id));
    }

    public ProductPage clickProductLinkById(int id) {
        logger.debug("Click on product with id \"" + id + "\"");
        click(getProductLocatorById(id));
        logger.info("Move to product page");
        return new ProductPage();
    }

    public SearchResultPage highlightProductById(int id) {
        highlightElement(getProductLocatorById(id));
        return this;
    }

    private By getProductLocatorById(int id) {
        return By.cssSelector(String.format(productCssLocator, id));
    }
}
