package com.epam.at.hw5.utils.Creators;

import com.epam.at.hw5.bo.Mail;

public abstract class MailCreator {
    public abstract Mail create();
}
