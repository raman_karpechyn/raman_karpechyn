package com.epam.at.hw5.configuration;

import com.epam.at.hw5.data.TestData;
import com.epam.at.hw5.services.LoginService;
import com.epam.at.hw5.services.MailService;
import com.epam.at.hw5.utils.Creators.MailCreator;
import com.epam.at.hw5.utils.Creators.UniqueMail;
import com.epam.at.hw5.impl.WebDriverSingleton;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.util.concurrent.TimeUnit;

public class Configuration {
    protected static MailCreator mailCreator;

    @BeforeClass
    public static void setUp() {
        WebDriverSingleton.getInstance().manage().window().maximize();
        WebDriverSingleton.getInstance().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        WebDriverSingleton.getInstance().get(TestData.getLink());

        mailCreator = new UniqueMail();
    }

    @AfterClass
    public static void setDown() throws Exception {
        try {
            clearMail();
            logOut();
        } catch (Exception ex) {
            throw ex;
        } finally {
            tearDown();
        }
    }

    private static void clearMail() {
        new MailService().goToInboxPage()
                         .deleteAllMails()
                         .goToSendPage()
                         .deleteAllMails()
                         .goToDraftPage()
                         .deleteAllMails();
    }

    private static void logOut() {
        new LoginService().logout();
    }

    private static void tearDown() {
        WebDriverSingleton.getInstance().quit();
    }
}
