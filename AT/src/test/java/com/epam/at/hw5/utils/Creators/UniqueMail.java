package com.epam.at.hw5.utils.Creators;

import com.epam.at.hw5.bo.Mail;
import com.epam.at.hw5.data.TestData;
import com.epam.at.hw5.utils.DataGenerator;

public class UniqueMail extends MailCreator {
    public Mail create() {
        return new Mail(TestData.getMailTo(), DataGenerator.getStringOfCurrentTime(), DataGenerator.getStringOfCurrentTime());
    }
}
