package com.epam.at.hw5.po;

import com.epam.at.hw5.po.common.Page;
import org.openqa.selenium.By;

public class HomePage extends Page {
    private final By loginField = By.id("mailbox:login");
    private final By passwordField = By.id("mailbox:password");
    private final By logInButton = By.id("mailbox:submit");

    public HomePage enterLogin(String login) {
        enterText(loginField, login);
        return this;
    }

    public HomePage enterPassword(String password) {
        enterText(passwordField, password);
        return this;
    }

    public MailBoxPage clickLoginButton() {
        click(logInButton);
        return new MailBoxPage();
    }
}
