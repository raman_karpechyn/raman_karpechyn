package com.epam.at.hw5.services;

import com.epam.at.hw5.bo.Mail;
import com.epam.at.hw5.bo.User;
import com.epam.at.hw5.po.MailBoxPage;
import com.epam.at.hw5.po.MailPage;
import com.epam.at.hw5.utils.Asserts;

public class MailService {
    public MailService goToInboxPage() {
        new MailBoxPage().clickInboxPageLink();
        return this;
    }

    public MailService goToDraftPage() {
        new MailBoxPage().clickDraftPageLink();
        return this;
    }

    public MailService goToSendPage() {
        new MailBoxPage().clickSendPageLink();
        return this;
    }

    public MailService createNewMail(Mail mail) {
        new MailBoxPage().clickCreateNewMailButton()
                         .enterMailTo(mail.getAddressee())
                         .enterSubject(mail.getSubject())
                         .enterMailBody(mail.getBody());
        return this;
    }

    public MailService openMailBySubject(String subject) {
        new MailBoxPage().clickMailLinkBySubject(subject);
        return this;
    }

    public MailService saveMailToDraft() {
        new MailPage().clickSaveMailToDraftButton();
        return this;
    }

    public MailService sendMail() {
        new MailPage().clickSendMailButton();
        return this;
    }

    public MailService deleteAllMails() {
        new MailBoxPage().deleteAllMails();
        return this;
    }

    public boolean isUserLogin(User user) {
        return new MailBoxPage().getUserEmail()
                                .equals(user.getEmail());
    }

    public boolean isMailSaved() {
        return new MailPage().isMailSavedToDraft();
    }

    public boolean isMailExist(String subject) {
        return new MailBoxPage().isMailWithSuchSubjectExist(subject);
    }

    public boolean isMailsEqual(Mail mail) {
        return Asserts.assertMailEquals(mail, getMailBySubject(mail.getSubject()));
    }

    private Mail getMailBySubject(String subject) {
        MailBoxPage box = new MailBoxPage();
        return new Mail(
                box.getMailToBySubject(subject),
                subject,
                box.getMailBodyBySubject(subject)
        );
    }
}
