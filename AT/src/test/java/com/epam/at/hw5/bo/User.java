package com.epam.at.hw5.bo;

public class User {
    private String login;
    private String password;
    private String email;

    public User(String email, String password) {
        this.email = email;
        this.password = password;
        this.login = email.replaceAll("@.*", "");
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }
}
