package com.epam.at.hw5.utils.Creators;

import com.epam.at.hw5.bo.Mail;

public class CustomMail extends MailCreator {
    private String mailTo;
    private String subject;
    private String body;

    public CustomMail(String mailTo, String subject, String body) {
        this.mailTo = mailTo;
        this.subject = subject;
        this.body = body;
    }

    public Mail create() {
        return new Mail(mailTo, subject, body);
    }
}
