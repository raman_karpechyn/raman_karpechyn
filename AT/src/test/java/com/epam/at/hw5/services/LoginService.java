package com.epam.at.hw5.services;

import com.epam.at.hw5.bo.User;
import com.epam.at.hw5.po.HomePage;
import com.epam.at.hw5.po.MailBoxPage;

public class LoginService {
    public MailService login(User user) {
        new HomePage().enterLogin(user.getLogin())
                      .enterPassword(user.getPassword())
                      .clickLoginButton();
        return new MailService();
    }

    public LoginService logout() {
        new MailBoxPage().clickLogoutLink();
        return this;
    }
}
