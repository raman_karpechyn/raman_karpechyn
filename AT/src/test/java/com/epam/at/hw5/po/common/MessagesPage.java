package com.epam.at.hw5.po.common;

import com.epam.at.hw5.po.HomePage;
import com.epam.at.hw5.po.MailBoxPage;
import com.epam.at.hw5.po.MailPage;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;

public abstract class MessagesPage extends Page {
    private final By userEmail = By.id("PH_user-email");
    private final By newMailButton = By.cssSelector("a[href^='/compose/?']");
    private final By inboxButton = By.cssSelector("a[href='/messages/inbox/']");
    private final By sentButton = By.cssSelector("a[href='/messages/sent/']");
    private final By draftsButton = By.cssSelector("a[href='/messages/drafts/']");
    private final By logoutLink = By.id("PH_logoutLink");
    private final By mailsBox = By.cssSelector("div[class='b-datalists']");

    protected final By bodyHTMLSelector = By.cssSelector("body");

    public MailBoxPage clickInboxPageLink() {
        click(inboxButton);
        waitForElementVisibility(mailsBox);
        return new MailBoxPage();
    }

    public MailBoxPage clickSendPageLink() {
        click(sentButton);
        waitForElementVisibility(mailsBox);
        return new MailBoxPage();
    }

    public MailBoxPage clickDraftPageLink() {
        int tryCount = 0;
        while (tryCount < 2) {
            try {
                click(draftsButton);
                tryCount = 2;
            } catch (StaleElementReferenceException ex) {
                tryCount++;
            }
        }
        waitForElementVisibility(mailsBox);
        return new MailBoxPage();
    }

    public MailPage clickCreateNewMailButton() {
        click(newMailButton);
        return new MailPage();
    }

    public String getUserEmail() {
        return getText(userEmail);
    }

    public HomePage clickLogoutLink() {
        click(logoutLink);
        return new HomePage();
    }
}
