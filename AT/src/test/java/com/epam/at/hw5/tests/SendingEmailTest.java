package com.epam.at.hw5.tests;

import com.epam.at.hw5.bo.User;
import com.epam.at.hw5.configuration.Configuration;
import com.epam.at.hw5.data.TestData;
import com.epam.at.hw5.bo.Mail;
import com.epam.at.hw5.services.LoginService;
import com.epam.at.hw5.services.MailService;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SendingEmailTest extends Configuration {
    @Test
    public void testSendEmail() {
        User user = new User(TestData.getMail(), TestData.getPassword());
        Mail mail = mailCreator.create();

        MailService mailService = new LoginService().login(user)
                                                    .createNewMail(mail)
                                                    .saveMailToDraft();

        Assert.assertTrue(mailService.isMailSaved());

        mailService.goToDraftPage()
                   .openMailBySubject(mail.getSubject())
                   .sendMail()
                   .goToDraftPage();

        Assert.assertFalse(mailService.isMailExist(mail.getSubject()));

        mailService.goToSendPage();

        Assert.assertTrue(mailService.isMailExist(mail.getSubject()));
        Assert.assertTrue(mailService.isMailsEqual(mail));
    }
}
