package com.epam.at.hw5.data;

public class TestData {
    private static String link = "https://mail.ru";
    private static String mail = "mq329u@mail.ru";
    private static String password = "HjV4fK";
    private static String mailTo = "raman_karpechyn@outlook.com";

    public static String getLink() {
        return link;
    }

    public static String getMail() {
        return mail;
    }

    public static String getPassword() {
        return password;
    }

    public static String getMailTo() {
        return mailTo;
    }
}
