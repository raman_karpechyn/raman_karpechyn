package com.epam.at.hw5.utils;

import com.epam.at.hw5.bo.Mail;

public class Asserts {
    public static boolean assertMailEquals(Mail mailOne, Mail mailTwo) {
        return mailOne.getAddressee().equals(mailTwo.getAddressee())
                && mailOne.getSubject().equals(mailTwo.getSubject())
                && mailOne.getBody().equals(mailTwo.getBody());
    }
}
