package com.epam.at.hw5.tests;

import com.epam.at.hw5.bo.User;
import com.epam.at.hw5.configuration.Configuration;
import com.epam.at.hw5.data.TestData;
import com.epam.at.hw5.services.LoginService;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginTest extends Configuration {
    @Test
    public void testLogin() {
        User user = new User(TestData.getMail(), TestData.getPassword());
        Assert.assertTrue(new LoginService().login(user)
                                            .isUserLogin(user));
    }
}
