package com.epam.at.hw5.po;

import com.epam.at.hw5.po.common.MessagesPage;
import org.openqa.selenium.By;

public class MailPage extends MessagesPage {
    private final By mailToField = By.cssSelector("textarea[data-original-name='To']");
    private final By subjectField = By.name("Subject");
    private final By mailBodyFrame = By.cssSelector("iframe[id^='toolkit-']");
    private final By saveToDraftButton = By.xpath("//div[@data-name='saveDraft']");
    private final By savedToDraftLabel = By.cssSelector("div[data-mnemo='saveStatus']");
    private final By sendMailButton = By.xpath("//div[@data-name='send']");
    private final By sentMailLabel = By.id("b-compose__sent");

    public MailPage enterMailTo(String email) {
        enterText(mailToField, email);
        return this;
    }

    public MailPage enterSubject(String subject) {
        enterText(subjectField, subject);
        return this;
    }

    public MailPage enterMailBody(String text) {
        waitForElementVisibility(mailBodyFrame);
        driver.switchTo().frame(driver.findElement(mailBodyFrame));
        clear(bodyHTMLSelector);
        enterText(bodyHTMLSelector, text);
        driver.switchTo().defaultContent();
        return this;
    }

    public MailPage clickSaveMailToDraftButton() {
        click(saveToDraftButton);
        return this;
    }

    public boolean isMailSavedToDraft() {
        waitForElementVisibility(savedToDraftLabel);
        return elementIsPresent(savedToDraftLabel);
    }

    public MailBoxPage clickSendMailButton() {
        click(sendMailButton);
        waitForElementVisibility(sentMailLabel);
        return new MailBoxPage();
    }
}
