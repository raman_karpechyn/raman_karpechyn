package com.epam.at.hw5.tests;

import com.epam.at.hw5.bo.User;
import com.epam.at.hw5.configuration.Configuration;
import com.epam.at.hw5.bo.Mail;
import com.epam.at.hw5.data.TestData;
import com.epam.at.hw5.services.LoginService;
import com.epam.at.hw5.services.MailService;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CreatingEmailTest extends Configuration {
    @Test
    public void testCreateEmail() {
        User user = new User(TestData.getMail(), TestData.getPassword());
        Mail mail = mailCreator.create();

        MailService mailService = new LoginService().login(user)
                                                    .createNewMail(mail)
                                                    .saveMailToDraft();

        Assert.assertTrue(mailService.isMailSaved());

        mailService.goToDraftPage();

        Assert.assertTrue(mailService.isMailExist(mail.getSubject()));
        Assert.assertTrue(mailService.isMailsEqual(mail));
    }
}
