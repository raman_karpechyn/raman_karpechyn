package com.epam.at.hw4.task.tests;

import com.epam.at.hw4.task.bo.User;
import com.epam.at.hw4.task.bo.Mail;
import com.epam.at.hw4.task.configuration.Configuration;
import com.epam.at.hw4.task.data.TestData;
import com.epam.at.hw4.task.services.LoginService;
import com.epam.at.hw4.task.services.MailService;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CreatingEmailTest extends Configuration {
    @Test
    public void testCreateEmail() {
        User user = new User(TestData.getMail(), TestData.getPassword());
        Mail mail = new Mail(TestData.getMailTo(),
                             TestData.getSubject(),
                             TestData.getText()
        );

        MailService mailService = new LoginService(driver).login(user)
                                                          .createNewMail(mail)
                                                          .saveMailToDraft();

        Assert.assertTrue(mailService.isMailSaved());

        mailService.goToDraftPage();

        Assert.assertTrue(mailService.isMailExist(mail.getSubject()));
        Assert.assertTrue(mailService.isMailsEqual(mail));
    }
}
