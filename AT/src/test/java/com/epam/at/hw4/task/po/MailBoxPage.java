package com.epam.at.hw4.task.po;

import com.epam.at.hw4.task.po.common.MessagesPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class MailBoxPage extends MessagesPage {
    private final By mailtoLabel = By.className("b-datalist__item__addr");
    private final By mailBodyLabel = By.className("b-datalist__item__subj__snippet");
    private final String mailWithSubjectSelector = "//a[@data-subject='%s']";

    public MailBoxPage(WebDriver driver) {
        super(driver);
    }

    public MailPage clickMailLinkBySubject(String subject) {
        click(getMassageLocatorBySubject(subject));
        return new MailPage(driver);
    }

    public String getMailToBySubject(String subject) {
        return getMailAttributeBySubject(subject, mailtoLabel);
    }

    public String getMailBodyBySubject(String subject) {
        return getMailAttributeBySubject(subject, mailBodyLabel);
    }

    public MailBoxPage deleteAllMails() {
        clear(bodyHTMLSelector);
        return this;
    }

    public boolean isMailWithSuchSubjectExist(String subject) {
        return elementIsPresent(getMassageLocatorBySubject(subject));
    }

    private String getMailAttributeBySubject(String mailSubject, By attributeLocator) {
        waitForElementVisibility(attributeLocator);
        return driver.findElement(getMassageLocatorBySubject(mailSubject))
                     .findElement(attributeLocator)
                     .getText();
    }

    private By getMassageLocatorBySubject(String subject) {
        return By.xpath(String.format(mailWithSubjectSelector, subject));
    }
}
