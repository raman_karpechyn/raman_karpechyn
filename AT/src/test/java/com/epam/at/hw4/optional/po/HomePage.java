package com.epam.at.hw4.optional.po;

import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;

public class HomePage {
    private final By loginField = By.id("mailbox:login");
    private final By passwordField = By.id("mailbox:password");
    private final By loginButton = By.id("mailbox:submit");

    public HomePage enterLogin(String login) {
        $(loginField).shouldBe(visible).setValue(login);
        return this;
    }

    public HomePage enterPassword(String password) {
        $(passwordField).shouldBe(visible).setValue(password);
        return this;
    }

    public InboxPage clickLoginButton() {
        $(loginButton).shouldBe(visible).click();
        return new InboxPage();
    }
}
