package com.epam.at.hw4.task.po.common;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class Page {
    protected final WebDriver driver;

    public Page(WebDriver driver) {
        this.driver = driver;
    }

    public WebDriver getDriver() {
        return driver;
    }

    protected void click(By locator) {
        waitForElementVisibility(locator);
        driver.findElement(locator).click();
    }

    protected void enterText(By locator, String text) {
        waitForElementVisibility(locator);
        driver.findElement(locator).sendKeys(text);
    }

    protected String getText(By locator) {
        waitForElementVisibility(locator);
        return driver.findElement(locator).getText();
    }

    protected void clear(By locator) {
        waitForElementVisibility(locator);
        driver.findElement(locator).sendKeys(Keys.chord(Keys.CONTROL, "a"));
        driver.findElement(locator).sendKeys(Keys.DELETE);
    }

    protected void waitForElementVisibility(By locator) {
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    protected boolean elementIsPresent(By locator) {
        return driver.findElements(locator).size() > 0;
    }
}
