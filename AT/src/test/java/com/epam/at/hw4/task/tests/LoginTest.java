package com.epam.at.hw4.task.tests;

import com.epam.at.hw4.task.bo.User;
import com.epam.at.hw4.task.configuration.Configuration;
import com.epam.at.hw4.task.data.TestData;
import com.epam.at.hw4.task.services.LoginService;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginTest extends Configuration {
    @Test
    public void testLogin() {
        User user = new User(TestData.getMail(), TestData.getPassword());
        Assert.assertTrue(new LoginService(driver).login(user)
                                                  .isUserLogin(user));
    }
}
