package com.epam.at.hw4.task.utils;

import com.epam.at.hw4.task.bo.Mail;

public class Asserts {
    public static boolean assertMailEquals(Mail mailOne, Mail mailTwo) {
        return mailOne.getAddressee().equals(mailTwo.getAddressee())
                && mailOne.getSubject().equals(mailTwo.getSubject())
                && mailOne.getBody().equals(mailTwo.getBody());
    }
}
