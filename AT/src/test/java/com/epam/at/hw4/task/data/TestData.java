package com.epam.at.hw4.task.data;

import com.epam.at.hw4.task.utils.DataGeneration;

public class TestData {
    private static String link = "https://mail.ru";
    private static String mail = "mq329u@mail.ru";
    private static String password = "HjV4fK";
    private static String mailTo = "raman_karpechyn@outlook.com";
    private static String text = "Some text.";

    public static String getLink() {
        return link;
    }

    public static String getMail() {
        return mail;
    }

    public static String getPassword() {
        return password;
    }

    public static String getMailTo() {
        return mailTo;
    }

    public static String getSubject() {
        return DataGeneration.getStringByCurrentTime();
    }

    public static String getText() {
        return text;
    }
}
