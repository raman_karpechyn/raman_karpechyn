package com.epam.at.hw4.optional.po;

import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;

public class InboxPage {
    private final By userEmail = By.id("PH_user-email");
    private final By logoutLink = By.id("PH_logoutLink");

    public String getUserEmail() {
        return $(userEmail).shouldBe(visible).getText();
    }

    public HomePage clickLogout() {
        $(logoutLink).click();
        return new HomePage();
    }
}
