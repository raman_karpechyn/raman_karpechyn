package com.epam.at.hw4.task.services;

import com.epam.at.hw4.task.bo.User;
import com.epam.at.hw4.task.po.HomePage;
import com.epam.at.hw4.task.po.MailBoxPage;
import org.openqa.selenium.WebDriver;

public class LoginService {
    protected final WebDriver driver;

    public LoginService(WebDriver driver) {
        this.driver = driver;
    }

    public MailService login(User user) {
        new HomePage(driver).enterLogin(user.getLogin())
                            .enterPassword(user.getPassword())
                            .clickLoginButton();
        return new MailService(driver);
    }

    public LoginService logout() {
        new MailBoxPage(driver).clickLogoutLink();
        return this;
    }
}
