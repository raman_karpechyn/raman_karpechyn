package com.epam.at.hw4.task.po.common;

import com.epam.at.hw4.task.po.HomePage;
import com.epam.at.hw4.task.po.MailBoxPage;
import com.epam.at.hw4.task.po.MailPage;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;

public abstract class MessagesPage extends Page {
    private final By userEmail = By.id("PH_user-email");
    private final By newMailButton = By.cssSelector("a[href^='/compose/?']");
    private final By inboxButton = By.cssSelector("a[href='/messages/inbox/']");
    private final By sentButton = By.cssSelector("a[href='/messages/sent/']");
    private final By draftsButton = By.cssSelector("a[href='/messages/drafts/']");
    private final By logoutLink = By.id("PH_logoutLink");
    private final By mailsBox = By.cssSelector("div[class='b-datalists']");

    protected final By bodyHTMLSelector = By.cssSelector("body");

    public MessagesPage(WebDriver driver) {
        super(driver);
    }

    public MailBoxPage clickInboxPageLink() {
        click(inboxButton);
        waitForElementVisibility(mailsBox);
        return new MailBoxPage(driver);
    }

    public MailBoxPage clickSendPageLink() {
        click(sentButton);
        waitForElementVisibility(mailsBox);
        return new MailBoxPage(driver);
    }

    public MailBoxPage clickDraftPageLink() {
        int tryCount = 0;
        while (tryCount < 2) {
            try {
                click(draftsButton);
                tryCount = 2;
            } catch (StaleElementReferenceException ex) {
                tryCount++;
            }
        }
        waitForElementVisibility(mailsBox);
        return new MailBoxPage(driver);
    }

    public MailPage clickCreateNewMailButton() {
        click(newMailButton);
        return new MailPage(driver);
    }

    public String getUserEmail() {
        return getText(userEmail);
    }

    public HomePage clickLogoutLink() {
        click(logoutLink);
        return new HomePage(driver);
    }
}
