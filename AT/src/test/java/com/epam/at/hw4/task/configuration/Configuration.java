package com.epam.at.hw4.task.configuration;

import com.epam.at.hw4.task.data.TestData;
import com.epam.at.hw4.task.services.LoginService;
import com.epam.at.hw4.task.services.MailService;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.util.concurrent.TimeUnit;

public class Configuration {
    private static final String CHROME_PATH = "src/test/resources/chromedriver.exe";
    protected static WebDriver driver;

    @BeforeClass
    public static void setUp() {
        if (driver == null) {
            System.setProperty("webdriver.chrome.driver", CHROME_PATH);
            driver = new ChromeDriver();
            driver.manage().window().maximize();
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            driver.get(TestData.getLink());
        }
    }

    @AfterClass
    public static void setDown() throws Exception {
        try {
            clearMail();
            logOut();
        } catch (Exception ex) {
            throw ex;
        } finally {
            tearDown();
        }
    }

    private static void clearMail() {
        new MailService(driver).goToInboxPage()
                               .deleteAllMails()
                               .goToSendPage()
                               .deleteAllMails()
                               .goToDraftPage()
                               .deleteAllMails();
    }

    private static void logOut() {
        new LoginService(driver).logout();
    }

    private static void tearDown() {
        if (driver != null) {
            driver.quit();
            driver = null;
        }
    }
}
