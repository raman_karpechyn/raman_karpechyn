package com.epam.at.hw4.optional.serveces;

import com.epam.at.hw4.optional.po.HomePage;
import com.epam.at.hw4.optional.po.InboxPage;
import com.epam.at.hw4.task.bo.User;

public class LoginService {
    public LoginService login(User user) {
        new HomePage()
                .enterLogin(user.getLogin())
                .enterPassword(user.getPassword())
                .clickLoginButton();
        return this;
    }

    public LoginService logout() {
        new InboxPage().clickLogout();
        return this;
    }

    public String getUserEmail() {
        return new InboxPage().getUserEmail();
    }
}
