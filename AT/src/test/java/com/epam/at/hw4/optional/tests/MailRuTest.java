package com.epam.at.hw4.optional.tests;

import com.codeborne.selenide.Configuration;
import com.epam.at.hw4.optional.serveces.LoginService;
import com.epam.at.hw4.task.bo.User;
import com.epam.at.hw4.task.data.TestData;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.open;

public class MailRuTest {
    @BeforeMethod
    public void Initialize() {
        Configuration.browser = "chrome";
        open(TestData.getLink());
    }

    @Test
    public void testLonin() {
        Assert.assertEquals(new LoginService().login(new User(TestData.getMail(), TestData.getPassword())).getUserEmail(), TestData.getMail());
    }

    @AfterMethod
    public void Close() {
        new LoginService().logout();
    }
}
