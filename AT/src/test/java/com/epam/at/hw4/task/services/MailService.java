package com.epam.at.hw4.task.services;

import com.epam.at.hw4.task.bo.Mail;
import com.epam.at.hw4.task.bo.User;
import com.epam.at.hw4.task.po.MailBoxPage;
import com.epam.at.hw4.task.po.MailPage;
import com.epam.at.hw4.task.utils.Asserts;
import org.openqa.selenium.WebDriver;

public class MailService {
    protected final WebDriver driver;

    public MailService(WebDriver driver) {
        this.driver = driver;
    }

    public MailService goToInboxPage() {
        new MailBoxPage(driver).clickInboxPageLink();
        return this;
    }

    public MailService goToDraftPage() {
        new MailBoxPage(driver).clickDraftPageLink();
        return this;
    }

    public MailService goToSendPage() {
        new MailBoxPage(driver).clickSendPageLink();
        return this;
    }

    public MailService createNewMail(Mail mail) {
        new MailBoxPage(driver).clickCreateNewMailButton()
                               .enterMailTo(mail.getAddressee())
                               .enterSubject(mail.getSubject())
                               .enterMailBody(mail.getBody());
        return this;
    }

    public MailService openMailBySubject(String subject) {
        new MailBoxPage(driver).clickMailLinkBySubject(subject);
        return this;
    }

    public MailService saveMailToDraft() {
        new MailPage(driver).clickSaveMailToDraftButton();
        return this;
    }

    public MailService sendMail() {
        new MailPage(driver).clickSendMailButton();
        return this;
    }

    public MailService deleteAllMails() {
        new MailBoxPage(driver).deleteAllMails();
        return this;
    }

    public boolean isUserLogin(User user) {
        return new MailBoxPage(driver).getUserEmail()
                                      .equals(user.getEmail());
    }

    public boolean isMailSaved() {
        return new MailPage(driver).isMailSavedToDraft();
    }

    public boolean isMailExist(String subject) {
        return new MailBoxPage(driver).isMailWithSuchSubjectExist(subject);
    }

    public boolean isMailsEqual(Mail mail) {
        return Asserts.assertMailEquals(mail, getMailBySubject(mail.getSubject()));
    }

    private Mail getMailBySubject(String subject) {
        MailBoxPage box = new MailBoxPage(driver);
        return new Mail(
                box.getMailToBySubject(subject),
                subject,
                box.getMailBodyBySubject(subject)
        );
    }
}
