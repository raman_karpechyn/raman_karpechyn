package com.epam.at.hw4.task.po;

import com.epam.at.hw4.task.po.common.Page;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomePage extends Page {
    private final By loginField = By.id("mailbox:login");
    private final By passwordField = By.id("mailbox:password");
    private final By logInButton = By.id("mailbox:submit");

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public HomePage enterLogin(String login) {
        enterText(loginField, login);
        return this;
    }

    public HomePage enterPassword(String password) {
        enterText(passwordField, password);
        return this;
    }

    public MailBoxPage clickLoginButton() {
        click(logInButton);
        return new MailBoxPage(driver);
    }
}
