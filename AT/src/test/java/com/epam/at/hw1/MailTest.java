package com.epam.at.hw1;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.util.concurrent.TimeUnit;

public class MailTest {
    protected static String link = "https://mail.ru";
    protected static String mail = "mq329u@mail.ru";
    protected static String login = "mq329u";
    protected static String password = "HjV4fK";
    protected static String mailTo = "raman_karpechyn@outlook.com";
    protected static String subject = Long.toString(System.currentTimeMillis());
    protected static String text = "Some text.";

    public static final String CHROME_PATH = "src/test/resources/chromedriver.exe";
    protected static WebDriver driver;

    @BeforeClass
    public static void setUp() {
        System.setProperty("webdriver.chrome.driver", CHROME_PATH);
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }

    @BeforeClass(dependsOnMethods = "setUp")
    public static void logIn() {
        driver.get(link);
        driver.findElement(By.id("mailbox:login")).sendKeys(login);
        driver.findElement(By.id("mailbox:password")).sendKeys(password);
        driver.findElement(By.id("mailbox:submit")).findElement(By.tagName("input")).click();
    }

    @AfterClass
    public static void clearMail() {
        driver.get("https://e.mail.ru/messages/inbox/");
        driver.findElement(By.cssSelector("body")).sendKeys(Keys.chord(Keys.CONTROL, "a"));
        driver.findElement(By.cssSelector("body")).sendKeys(Keys.DELETE);
        driver.get("https://e.mail.ru/messages/sent/");
        driver.findElement(By.cssSelector("body")).sendKeys(Keys.chord(Keys.CONTROL, "a"));
        driver.findElement(By.cssSelector("body")).sendKeys(Keys.DELETE);
        driver.get("https://e.mail.ru/messages/drafts/");
        driver.findElement(By.cssSelector("body")).sendKeys(Keys.chord(Keys.CONTROL, "a"));
        driver.findElement(By.cssSelector("body")).sendKeys(Keys.DELETE);
    }

    @AfterClass(dependsOnMethods = "clearMail")
    public static void logOut() {
        driver.findElement(By.id("PH_logoutLink")).click();
    }

    @AfterClass(dependsOnMethods = "logOut")
    public static void tearDown() {
        driver.quit();
    }

    public static void waitForElementVisibility(By locator) {
        new WebDriverWait(driver, 5).until(ExpectedConditions.visibilityOfElementLocated(locator));
    }
}
