package com.epam.at.hw1;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CreatingEmailTest extends MailTest {
    @Test
    public void testCreateEmail() {
        String newMailBtnCSS = "a[href^='/compose/?']";
        waitForElementVisibility(By.cssSelector(newMailBtnCSS));
        driver.findElement(By.cssSelector(newMailBtnCSS)).click();

        driver.findElement(By.cssSelector("textarea[data-original-name='To']")).sendKeys(mailTo);
        driver.findElement(By.name("Subject")).sendKeys(subject);
        driver.switchTo().frame(driver.findElement(By.cssSelector("iframe[id^='toolkit-']")));
        driver.findElement(By.cssSelector("body")).sendKeys(Keys.chord(Keys.CONTROL, "a"));
        driver.findElement(By.cssSelector("body")).sendKeys(Keys.DELETE);
        driver.findElement(By.cssSelector("body")).sendKeys(text);
        driver.switchTo().defaultContent();

        driver.findElement(By.xpath("//div[@data-name='saveDraft']")).click();
        waitForElementVisibility(By.cssSelector("div[data-mnemo='saveStatus']"));

        driver.findElement(By.cssSelector("a[href='/messages/drafts/']")).click();

        String mailInfoXPath = String.format("//a[@data-subject='%s']", subject);
        waitForElementVisibility(By.xpath(mailInfoXPath));
        WebElement element = driver.findElement(By.xpath(mailInfoXPath));

        String actualMailTo = element.findElement(By.className("b-datalist__item__addr")).getText();
        String actualText = element.findElement(By.className("b-datalist__item__subj__snippet")).getText();
        String actualSubject = element.findElement(By.className("b-datalist__item__subj")).getText().replace(actualText, "");

        Assert.assertEquals(actualMailTo, mailTo);
        Assert.assertEquals(actualSubject, subject);
        Assert.assertEquals(actualText, text);
    }
}
