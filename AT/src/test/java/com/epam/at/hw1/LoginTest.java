package com.epam.at.hw1;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginTest extends MailTest {
    @Test
    public void testLogin() {
        By emailLocator = By.id("PH_user-email");
        waitForElementVisibility(emailLocator);
        String actual = driver.findElement(emailLocator).getText();
        String expected = mail;

        Assert.assertEquals(actual, expected);
    }
}
