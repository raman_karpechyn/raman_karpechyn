Feature: Add to Cart

  Scenario: Running a Full Text Product Search
    Given I open eBay Page
    When I search the product "Nintendo"
    And I click on the "1" product
    And I save selected product
    And I click on Add to Cart button
    Then Saved product should be equal with product in the cart
