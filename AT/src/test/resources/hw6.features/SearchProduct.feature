Feature: Search product

  Background:
    Given I open eBay Page

  Scenario: Running a Full Text Product Search
    When I search the product "Samsung"
    Then The "1" product on the Search Result page should contains query "Samsung"

  Scenario Outline: Running a Full Text Product Search
    When I search the product "<query>"
    Then The "1" product on the Search Result page should contains query "<query>"

    Examples:
      | query               |
      | MacBook             |
      | Pride and Prejudice |