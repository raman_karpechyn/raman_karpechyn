package com.epam.training.hw4.task1;

import org.junit.Assert;
import org.junit.Test;

public class CreditCardTest {
    @Test
    public void testIncreaseBalance() {
        double balance = -9.9d;
        double increaseValue = 900.1;

        CreditCard creditCard = new CreditCard("John", "Smith", balance);
        creditCard.increaseBalance(increaseValue);
        double result = creditCard.getBalance();
        Assert.assertEquals(balance + increaseValue, result, 0);
    }

    @Test
    public void testDecreaseBalance() {
        double balance = 9.9d;
        double decreaseValue = 9.9d;

        CreditCard creditCard = new CreditCard("John", "Smith", balance);
        creditCard.decreaseBalance(decreaseValue);
        double result = creditCard.getBalance();
        Assert.assertEquals(balance - decreaseValue, result, 0);
    }


    @Test
    public void testDecreaseBalanceWithNegativeBalanceResult() {
        double balance = 97.98d;
        double decreaseValue = 97.99d;

        CreditCard creditCard = new CreditCard("John", "Smith", balance);
        creditCard.decreaseBalance(decreaseValue);
        double result = creditCard.getBalance();
        Assert.assertEquals(balance - decreaseValue, result, 0);
    }
}