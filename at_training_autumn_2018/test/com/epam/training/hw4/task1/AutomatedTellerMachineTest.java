package com.epam.training.hw4.task1;

import org.junit.Assert;
import org.junit.Test;

public class AutomatedTellerMachineTest {
    @Test
    public void testRechargeAccountForCreditCard() {
        int atmID = 0;
        double balance = -911.59d;
        double increaseValue = 69.3d;

        AutomatedTellerMachine atm = new AutomatedTellerMachine(atmID);
        CreditCard creditCard = new CreditCard("John", "Smith", balance);
        atm.rechargeAccount(creditCard, increaseValue);
        double result = creditCard.getBalance();
        Assert.assertEquals(balance + increaseValue, result, 0);
    }

    @Test
    public void testRechargeAccountForDebitCard() {
        int atmID = 1;
        double balance = 39.49d;
        double increaseValue = 9.9d;

        AutomatedTellerMachine atm = new AutomatedTellerMachine(atmID);
        DebitCard debitCard = new DebitCard("John", "Smith", balance);
        atm.rechargeAccount(debitCard, increaseValue);
        double result = debitCard.getBalance();
        Assert.assertEquals(balance + increaseValue, result, 0);
    }


    @Test
    public void testWithdrawCashForCreditCard() {
        int atmID = 4;
        double balance = -14.54d;
        double decreaseValue = 33.3d;

        AutomatedTellerMachine atm = new AutomatedTellerMachine(atmID);
        CreditCard creditCard = new CreditCard("John", "Smith", balance);
        atm.withdrawСash(creditCard, decreaseValue);
        double result = creditCard.getBalance();
        Assert.assertEquals(balance - decreaseValue, result, 0);
    }

    @Test
    public void testWithdrawCashForDebitCardWithPositiveBalanceResult() {
        int atmID = 6;
        double balance = 459d;
        double decreaseValue = 33.3d;

        AutomatedTellerMachine atm = new AutomatedTellerMachine(atmID);
        DebitCard debitCard = new DebitCard("John", "Smith", balance);
        atm.withdrawСash(debitCard, decreaseValue);
        double result = debitCard.getBalance();
        Assert.assertEquals(balance - decreaseValue, result, 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testWithdrawCashForDebitCardWithNegativeBalanceResult() {
        int atmID = 7;
        double balance = 23d;
        double decreaseValue = 33.3d;

        AutomatedTellerMachine atm = new AutomatedTellerMachine(atmID);
        DebitCard debitCard = new DebitCard("John", "Smith", balance);
        atm.withdrawСash(debitCard, decreaseValue);
    }
}