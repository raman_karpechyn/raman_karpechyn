package com.epam.training.hw4.task1;

import org.junit.Assert;
import org.junit.Test;

public class DebitCardTest {
    @Test
    public void testIncreaseBalance() {
        double balance = -9.9d;
        double increaseValue = 900.1;

        DebitCard debitCard = new DebitCard("John", "Smith", balance);
        debitCard.increaseBalance(increaseValue);
        double result = debitCard.getBalance();
        Assert.assertEquals(balance + increaseValue, result, 0);
    }

    @Test
    public void testDecreaseBalance() {
        double balance = 9.9d;
        double decreaseValue = 9.9d;

        DebitCard debitCard = new DebitCard("John", "Smith", balance);
        debitCard.decreaseBalance(decreaseValue);
        double result = debitCard.getBalance();
        Assert.assertEquals(balance - decreaseValue, result, 0);
    }


    @Test(expected = IllegalArgumentException.class)
    public void testDecreaseBalanceWithNegativeBalanceResult() {
        DebitCard debitCard = new DebitCard("John", "Smith", 97.98d);
        debitCard.decreaseBalance(97.99d);
    }
}