package com.epam.training.hw4.task2;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

public class SortingContextTest {
    @Test
    public void testSortingContext() {
        int[] arrayForBubbleSort = new int[]{45, 2, 7, -3, 25, 5, 5};
        int[] arrayForSelectionSort = Arrays.copyOf(arrayForBubbleSort, arrayForBubbleSort.length);
        int[] controlArray = Arrays.copyOf(arrayForBubbleSort, arrayForBubbleSort.length);
        Arrays.sort(controlArray);

        Sorter sorter = new BubbleSort();
        SortingContext context = new SortingContext(sorter);
        context.execute(arrayForBubbleSort);
        Assert.assertArrayEquals(controlArray, arrayForBubbleSort);

        sorter = new SelectionSort();
        context.setSorter(sorter);
        context.execute(arrayForSelectionSort);
        Assert.assertArrayEquals(controlArray, arrayForSelectionSort);
    }
}
