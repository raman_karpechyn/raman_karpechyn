package com.epam.training.hw5;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

public class TextProcessorTest {
    private String sentence;
    private String fileName;
    private String emptySentence;
    private ArrayList<String> processedSentence;
    private ArrayList<String> processedEmptySentence;

    @Before
    public void initialize() {
        sentence = "Once upon a time a Wolf was lapping at a spring on a hillside, when," +
                " looking up, what should he see but a Lamb just beginning to drink a little lower down. ";
        fileName = "hw5_text_1.txt";
        emptySentence = "";
        processedSentence = new ArrayList<>(Arrays.asList("A: a at", "B: beginning but", "D: down drink",
                "H: he hillside", "J: just", "L: Lamb lapping little looking lower", "O: on Once",
                "S: see should spring", "T: time to", "U: up upon", "W: was what when Wolf"));
        processedEmptySentence = new ArrayList<>(Arrays.asList(": "));

    }

    @Test
    public void testReadTextFromFile() {
        String result = TextProcessor.readTextFromFile(fileName);

        System.out.println(sentence);
        System.out.println(result);

        Assert.assertEquals(sentence, result);
    }

    @Test
    public void testReadTextFromFileWithIncorrectName() {
        String result = TextProcessor.readTextFromFile(emptySentence);

        Assert.assertEquals(emptySentence, result);
    }

    @Test
    public void testProcessText() {
        System.out.println(sentence);
        ArrayList<String> result = TextProcessor.processText(sentence);

        printArrayList(processedSentence);
        printArrayList(result);

        Assert.assertEquals(processedSentence, result);
    }

    @Test
    public void testProcessTextWithEmptyString() {
        ArrayList<String> result = TextProcessor.processText(emptySentence);

        Assert.assertEquals(processedEmptySentence, result);
    }

    private void printArrayList(ArrayList<String> array) {
        for (String x : array) {
            System.out.print(x + " ");
        }
        System.out.println();
    }
}