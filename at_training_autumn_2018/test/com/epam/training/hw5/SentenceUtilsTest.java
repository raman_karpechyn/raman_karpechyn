package com.epam.training.hw5;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class SentenceUtilsTest {
    private String sentence;
    private String sentenceWithoutPunctuators;
    private String firstCharInSentence;
    private String emptySentence;
    private ArrayList<String> splitSentence;
    private ArrayList<String> splitSentenceAlphabetically;
    private ArrayList<String> duplicatedArrayList;
    private ArrayList<String> removedDuplicate;
    private ArrayList<String> collectedWords;
    private ArrayList<String> emptyArrayList;

    @Before
    public void initialize() {
        sentence = "I didn't have' Cat have time, Cat: to-get lost changed:" +
                " do i was have has Do Iron already! Late.";
        sentenceWithoutPunctuators = "I didn't have' Cat have time Cat to-get" +
                " lost changed do i was have has Do Iron already Late";
        firstCharInSentence = "I";
        emptySentence = "";
        splitSentence = new ArrayList<>(Arrays.asList("I", "didn't", "have'",
                "Cat", "have", "time", "Cat", "to-get", "lost", "changed", "do", "i",
                "was", "have", "has", "Do", "Iron", "already", "Late"));
        splitSentenceAlphabetically = new ArrayList<>(Arrays.asList("already",
                "Cat", "Cat", "changed", "didn't", "do", "Do", "has", "have", "have",
                "have'", "I", "i", "Iron", "Late", "lost", "time", "to-get", "was"));
        duplicatedArrayList = new ArrayList<>(Arrays.asList("I", "didn't", "have'",
                "Cat", "have", "time", "Cat", "to-get", "lost", "changed", "do", "i",
                "was", "have", "has", "Do", "Iron", "already", "Late"));
        removedDuplicate = new ArrayList<>(Arrays.asList("I", "didn't", "have'",
                "Cat", "have", "time", "to-get", "lost", "changed", "do", "was", "has",
                "Iron", "already", "Late"));
        collectedWords = new ArrayList<>(Arrays.asList("I i Iron", "didn't do Do",
                "have' have have has", "Cat Cat changed", "time to-get", "lost Late",
                "was", "already"));
        emptyArrayList = new ArrayList<>(Collections.singletonList(""));
    }

    @Test
    public void testSplitSentenceToWordsAndSortAlphabetically() {
        ArrayList<String> result = SentenceUtils.splitSentenceToWordsAndSortAlphabetically(sentence);

        System.out.println(sentence);
        printArrayList(splitSentenceAlphabetically);
        printArrayList(result);

        Assert.assertEquals(splitSentenceAlphabetically, result);
    }

    @Test
    public void testSplitSentenceToWordsAndSortAlphabeticallyWithEmptyString() {
        ArrayList<String> result = SentenceUtils.splitSentenceToWordsAndSortAlphabetically(emptySentence);

        Assert.assertEquals(emptyArrayList, result);
    }

    @Test
    public void testSplitSentenceToWords() {
        ArrayList<String> result = SentenceUtils.splitSentenceToWords(sentence);

        System.out.println(sentence);
        printArrayList(splitSentence);
        printArrayList(result);

        Assert.assertEquals(splitSentence, result);
    }

    @Test
    public void testSplitSentenceToWordsWithEmptyString() {
        ArrayList<String> result = SentenceUtils.splitSentenceToWords(emptySentence);

        Assert.assertEquals(emptyArrayList, result);
    }

    @Test
    public void testRemoveAllPunctuators() {
        String result = SentenceUtils.removeAllPunctuators(sentence);

        System.out.println(sentence);
        System.out.println(sentenceWithoutPunctuators);
        System.out.println(result);

        Assert.assertEquals(sentenceWithoutPunctuators, result);
    }

    @Test
    public void testRemoveAllPunctuatorsWithEmptyString() {
        String result = SentenceUtils.removeAllPunctuators(emptySentence);

        Assert.assertEquals(emptySentence, result);
    }

    @Test
    public void testRemoveAllDuplicatesCaseInsensitive() {
        printArrayList(duplicatedArrayList);

        SentenceUtils.removeAllDuplicatesCaseInsensitive(duplicatedArrayList);

        printArrayList(removedDuplicate);
        printArrayList(duplicatedArrayList);

        Assert.assertEquals(removedDuplicate, duplicatedArrayList);
    }

    @Test
    public void testRemoveAllDuplicatesCaseInsensitiveWithEmptyArrayList() {
        ArrayList<String> copy = new ArrayList<>(emptyArrayList);
        SentenceUtils.removeAllDuplicatesCaseInsensitive(emptyArrayList);

        Assert.assertEquals(copy, emptyArrayList);
    }

    @Test
    public void testCollectWordsByFirstCharacter() {
        printArrayList(duplicatedArrayList);

        SentenceUtils.collectWordsByFirstCharacter(duplicatedArrayList);

        printArrayList(collectedWords);
        printArrayList(duplicatedArrayList);

        Assert.assertEquals(collectedWords, duplicatedArrayList);
    }

    @Test
    public void testCollectWordsByFirstCharacterWithEmptyArrayList() {
        ArrayList<String> copy = new ArrayList<>(emptyArrayList);
        SentenceUtils.collectWordsByFirstCharacter(emptyArrayList);

        Assert.assertEquals(copy, emptyArrayList);
    }

    @Test
    public void testGetFirstCharInString() {
        String result = SentenceUtils.getFirstCharInString(sentence);

        System.out.println(sentence);
        System.out.println(firstCharInSentence);
        System.out.println(result);

        Assert.assertEquals(firstCharInSentence, result);
    }

    @Test
    public void testGetFirstCharInStringWithEmptyString() {
        String result = SentenceUtils.getFirstCharInString(emptySentence);

        Assert.assertEquals(emptySentence, result);
    }

    private void printArrayList(ArrayList<String> array) {
        for (String x : array) {
            System.out.print(x + " ");
        }
        System.out.println();
    }
}