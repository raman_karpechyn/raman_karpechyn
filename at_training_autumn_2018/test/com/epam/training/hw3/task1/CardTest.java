package com.epam.training.hw3.task1;

import org.junit.Assert;
import org.junit.Test;

public class CardTest {
    @Test
    public void testGetFullName() {
        String firstName = "John";
        String middleName = "George";
        String lastName = "Smith";
        String expect = String.format("%s %s %s", firstName, middleName, lastName);
        Card card = new Card(firstName, middleName, lastName);
        String result = card.getFullName();
        Assert.assertEquals(expect, result);
    }

    @Test
    public void testGetFullNameWithoutMiddleName() {
        String firstName = "John";
        String lastName = "Smith";
        String expect = String.format("%s %s", firstName, lastName);
        Card card = new Card(firstName, lastName);
        String result = card.getFullName();
        Assert.assertEquals(expect, result);
    }

    @Test
    public void testGetConversionBalance() {
        double balance = 99.9d;
        double conversionRate = 0.8;
        Card card = new Card("John", "Smith", balance);
        double result = card.getConversionBalance(conversionRate);
        Assert.assertEquals(balance * conversionRate, result, 0);
    }

    @Test
    public void testIncreaseBalance() {
        double balance = -9.9d;
        double increaseValue = 900.1;
        Card card = new Card("John", "Smith", balance);
        card.increaseBalance(increaseValue);
        double result = card.getBalance();
        Assert.assertEquals(balance + increaseValue, result, 0);
    }

    @Test
    public void testDecreaseBalance() {
        double balance = -9.9d;
        double decreaseValue = 10;
        Card card = new Card("John", "Smith", balance);
        card.decreaseBalance(decreaseValue);
        double result = card.getBalance();
        Assert.assertEquals(balance - decreaseValue, result, 0);
    }
}