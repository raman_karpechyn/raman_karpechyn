package com.epam.training.hw7.task1;

import org.junit.Assert;
import org.junit.Test;

public class ProgramTest {

    @Test
    public void testSortArrayBySumWithPositiveArray() {
        Integer[] expected = {0, 100, 101, 13, 22, 34, 25, 9, 27, 72, 7110, 1901, 77, 87, 79};
        Integer[] actual = {79, 9, 100, 27, 72, 13, 0, 34, 77, 7110, 101, 22, 1901, 25, 87};
        Program.sortArrayBySum(actual);
        Assert.assertArrayEquals(expected, actual);
    }

    @Test
    public void testSortArrayBySumWithNegativeArray() {
        Integer[] expected = {-0, -100, -101, -13, -22, -34, -25, -9, -27, -72, -7110, -1901, -77, -87, -79};
        Integer[] actual = {-79, -9, -100, -27, -72, -13, -0, -34, -77, -7110, -101, -22, -1901, -25, -87};
        Program.sortArrayBySum(actual);
        Assert.assertArrayEquals(expected, actual);
    }

    @Test
    public void testSortArrayBySumWithMixedArray() {
        Integer[] expected = {100, -101, -13, 13, -22, 34, 25, -9, -27, -72, 7110, -1901, -77, 87, -79};
        Integer[] actual = {-79, -9, 100, -27, -72, -13, 13, 34, -77, 7110, -101, -22, -1901, 25, 87};
        Program.sortArrayBySum(actual);
        Assert.assertArrayEquals(expected, actual);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSortArrayBySumWithArrayOfNulls() {
        Integer[] array = {null, null, null, 3};
        Program.sortArrayBySum(array);
    }
}