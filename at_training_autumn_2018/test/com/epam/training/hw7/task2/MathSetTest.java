package com.epam.training.hw7.task2;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class MathSetTest {
    private MathSet<Integer> firstSet;
    private MathSet<Integer> secondSet;

    @Before
    public void initialize() {
        firstSet = new MathSet<>();
        firstSet.add(-1);
        firstSet.add(2);
        firstSet.add(3);
        firstSet.add(4);
        secondSet = new MathSet<>();
        secondSet.add(3);
        secondSet.add(4);
        secondSet.add(5);
        secondSet.add(6);
    }

    @Test
    public void testGetUnion() {
        MathSet<Integer> expected = new MathSet<>();
        expected.add(-1);
        expected.add(2);
        expected.add(3);
        expected.add(4);
        expected.add(5);
        expected.add(6);
        firstSet.getUnion(secondSet);
        Assert.assertEquals(expected, firstSet);
    }

    @Test
    public void testGetIntersection() {
        MathSet<Integer> expected = new MathSet<>();
        expected.add(3);
        expected.add(4);
        firstSet.getIntersection(secondSet);
        Assert.assertEquals(expected, firstSet);
    }

    @Test
    public void testGetComplement() {
        MathSet<Integer> expected = new MathSet<>();
        expected.add(-1);
        expected.add(2);
        firstSet.getComplement(secondSet);
        Assert.assertEquals(expected, firstSet);
    }

    @Test
    public void testGetSymmetricDifference() {
        MathSet<Integer> expected = new MathSet<>();
        expected.add(-1);
        expected.add(2);
        expected.add(5);
        expected.add(6);
        firstSet.getSymmetricDifference(secondSet);
        Assert.assertEquals(expected, firstSet);
    }
}