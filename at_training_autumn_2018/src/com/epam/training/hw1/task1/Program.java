package com.epam.training.hw1.task1;

public class Program {
    public static void main(String[] args) {
        if (args.length != 4) {
            System.err.println("There are must be a four command-line arguments!");
        } else {
            int a, p;
            double m1, m2;

            try {
                a = Integer.parseInt(args[0]);
                p = Integer.parseInt(args[1]);
                m1 = Double.parseDouble(args[2]);
                m2 = Double.parseDouble(args[3]);
            } catch (NumberFormatException e) {
                System.err.println("Invalid command-line argument(-s)!");
                return;
            }

            double g;
            try {
                g = Function.getGravitationalAcceleration(a, p, m1, m2);
            } catch (IllegalArgumentException e) {
                System.err.printf("Invalid command-line argument(-s)!%n%s", e.getMessage());
                return;
            }
            System.out.printf("Gravitational acceleration is %f", g);
        }
    }
}