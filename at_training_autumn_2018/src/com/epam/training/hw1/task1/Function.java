package com.epam.training.hw1.task1;

public class Function {
    public static double getGravitationalAcceleration(int a, int p, double m1, double m2) {
        argumentValidation(p, m1, m2);

        return 4 * Math.pow(Math.PI, 2) * (Math.pow(a, 3) / (Math.pow(p, 2) * (m1 + m2)));
    }

    private static void argumentValidation(int p, double m1, double m2) {
        if (p == 0 || m1 + m2 == 0) {
            throw new IllegalArgumentException("Division by zero.");
        }
        if (Double.isNaN(m1) || Double.isNaN(m2)) {
            throw new IllegalArgumentException("Argument Not a Number.");
        }
    }
}