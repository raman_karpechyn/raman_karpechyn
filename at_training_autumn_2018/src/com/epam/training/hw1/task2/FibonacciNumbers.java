package com.epam.training.hw1.task2;

public class FibonacciNumbers {
    public static int[] getFibonacciNumbersThroughWhile(int n) {
        argumentValidation(n);

        int fibonacciFirst = 0;
        int fibonacciSecond = 1;
        int[] fibonacciNumbers = new int[n];

        int i = 0;
        while (i < n) {
            fibonacciNumbers[i] = fibonacciFirst + fibonacciSecond;
            fibonacciFirst = fibonacciSecond;
            fibonacciSecond = fibonacciNumbers[i];
            i++;
        }

        return fibonacciNumbers;
    }

    public static int[] getFibonacciNumbersThroughDoWhile(int n) {
        argumentValidation(n);

        int fibonacciFirst = 0;
        int fibonacciSecond = 1;
        int[] fibonacciNumbers = new int[n];

        int i = 0;
        do {
            fibonacciNumbers[i] = fibonacciFirst + fibonacciSecond;
            fibonacciFirst = fibonacciSecond;
            fibonacciSecond = fibonacciNumbers[i];
            i++;
        }
        while (i < n);

        return fibonacciNumbers;
    }

    public static int[] getFibonacciNumbersThroughFor(int n) {
        argumentValidation(n);

        int fibonacciFirst = 0;
        int fibonacciSecond = 1;
        int[] fibonacciNumbers = new int[n];

        for (int i = 0; i < n; i++) {
            fibonacciNumbers[i] = fibonacciFirst + fibonacciSecond;
            fibonacciFirst = fibonacciSecond;
            fibonacciSecond = fibonacciNumbers[i];
        }

        return fibonacciNumbers;
    }

    private static void argumentValidation(int value) {
        if (value <= 0) {
            throw new IllegalArgumentException("Argument must be positive.");
        }
    }
}
