package com.epam.training.hw1.task2;

public class Program {
    public static void main(String[] args) {
        if (args.length != 3) {
            System.err.println("There are must be a three command-line arguments!");
            return;
        }

        int algorithmId, loopType, n;
        try {
            algorithmId = Integer.parseInt(args[0]);
            loopType = Integer.parseInt(args[1]);
            n = Integer.parseInt(args[2]);
        } catch (NumberFormatException e) {
            System.err.println("Invalid command-line argument(-s)!");
            return;
        }

        if (algorithmId == 1) {
            getFibonacci(loopType, n);
        } else if (algorithmId == 2) {
            getFactorial(loopType, n);
        } else {
            System.err.println("Invalid algorithm id!");
        }

    }

    private static void getFibonacci(int loopType, int n) {
        switch (loopType) {
            case 1:
                printFibonacci(FibonacciNumbers.getFibonacciNumbersThroughWhile(n));
                break;
            case 2:
                printFibonacci(FibonacciNumbers.getFibonacciNumbersThroughDoWhile(n));
                break;
            case 3:
                printFibonacci(FibonacciNumbers.getFibonacciNumbersThroughFor(n));
                break;
            default:
                System.err.println("Invalid cycle id!");
                break;
        }
    }

    private static void getFactorial(int loopType, int n) {
        switch (loopType) {
            case 1:
                System.out.printf("Factorial for %d is %d", n, Factorial.getFactorialThroughWhile(n));
                break;
            case 2:
                System.out.printf("Factorial for %d is %d", n, Factorial.getFactorialThroughDoWhile(n));
                break;
            case 3:
                System.out.printf("Factorial for %d is %d", n, Factorial.getFactorialThroughFor(n));
                break;
            default:
                System.err.println("Invalid cycle id!");
                break;
        }
    }

    private static void printFibonacci(int[] fibonacciNumbers) {
        System.out.printf("Fibonacci numbers for %d%n", fibonacciNumbers.length);
        for (int x : fibonacciNumbers) {
            System.out.println(x);
        }
    }
}
