package com.epam.training.hw1.task2;

public class Factorial {
    public static int getFactorialThroughWhile(int n) {
        argumentValidation(n);

        int factorial = 1;
        int i = 2;
        while (i <= n) {
            factorial *= i++;
        }

        return factorial;
    }

    public static int getFactorialThroughDoWhile(int n) {
        argumentValidation(n);

        int factorial = 1;
        int i = 1;
        do {
            factorial *= i++;
        }
        while (i <= n);

        return factorial;
    }

    public static int getFactorialThroughFor(int n) {
        argumentValidation(n);

        int factorial = 1;
        for (int i = 2; i <= n; i++) {
            factorial *= i;
        }

        return factorial;
    }

    private static void argumentValidation(int value) {
        if (value < 0) {
            throw new IllegalArgumentException("Argument must be greater then zero.");
        }
    }
}
