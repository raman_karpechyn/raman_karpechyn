package com.epam.training.hw3.task1;

public class Card {
    private String firstName;
    private String middleName;
    private String lastName;
    private double balance;

    private static final String NO_MIDDLE_NAME = "";
    private static final byte DEFAULT_BALANCE = 0;


    public Card(String firstName, String middleName, String lastName, double balance) {
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.balance = balance;
    }

    public Card(String firstName, String lastName, double balance) {
        this(firstName, NO_MIDDLE_NAME, lastName, balance);
    }

    public Card(String firstName, String middleName, String lastName) {
        this(firstName, middleName, lastName, DEFAULT_BALANCE);
    }

    public Card(String firstName, String lastName) {
        this(firstName, NO_MIDDLE_NAME, lastName, DEFAULT_BALANCE);
    }


    public String getFirstName() {
        return firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFullName() {
        return String.format("%s %s%s%s", getFirstName(), getMiddleName(), (getMiddleName().length() == 0) ? "" : " ", getLastName());
    }


    public double getBalance() {
        return balance;
    }

    public double getConversionBalance(double conversionRate) {
        double temp = getBalance() * conversionRate;
        return temp;
    }

    public void increaseBalance(double increaseValue) {
        double temp = getBalance() + increaseValue;
        balance = temp;
    }

    public void decreaseBalance(double decreaseValue) {
        double temp = getBalance() - decreaseValue;
        balance = temp;
    }
}