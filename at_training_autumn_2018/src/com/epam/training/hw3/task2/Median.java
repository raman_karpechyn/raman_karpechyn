package com.epam.training.hw3.task2;

import java.util.Arrays;

public class Median {
    private Median() {
    }

    public static float median(int[] array) {
        double[] temp = new double[array.length];
        for (int i = 0; i < array.length; i++) {
            temp[i] = array[i];
        }
        return (float) calculateMedian(temp);
    }

    public static double median(double[] array) {
        double[] temp = Arrays.copyOf(array, array.length);
        return calculateMedian(temp);
    }

    private static double calculateMedian(double[] array) {
        double median;
        int arrayLength = array.length;

        Arrays.sort(array);
        if (arrayLength % 2 == 0)
            median = (array[arrayLength / 2 - 1] + array[arrayLength / 2]) / 2d;
        else
            median = array[arrayLength / 2];
        return median;
    }
}
