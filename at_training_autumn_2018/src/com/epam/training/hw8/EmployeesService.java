package com.epam.training.hw8;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class EmployeesService {
    public static void addEmployee(int id) {
        if (EmployeeSingleton.getInstance().getEmployee(id) != null) {
            System.out.println("Employee with such ID already exist!");
            return;
        }

        System.out.println("Enter Employee FIRST NAME:");
        String fname = ScannerSingleton.getInstance().nextLine();

        System.out.println("Enter Employee LAST NAME:");
        String lname = ScannerSingleton.getInstance().nextLine();

        if (EmployeeSingleton.getInstance().add(new Employee(id, fname, lname))) {
            System.out.println("Employee successfully added.");
        } else {
            System.out.println("Unexpected error occurred. Employee does not added.");
        }
    }

    public static void removeEmployee(int id) {
        if (EmployeeSingleton.getInstance().removeEmployee(id)) {
            System.out.println("Employee successfully removed.");
        } else {
            System.out.println("There is no Employee with such ID.");
        }
    }

    public static void saveEmployeesTableToDisc() {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("Employees.dat"))) {
            oos.writeObject(EmployeeSingleton.getInstance());
            System.out.println("Employees table successfully saved.");
        } catch (Exception e) {
            System.out.println("Unexpected error occurred. Employees table does not saved.");
        }
    }

    public static Employees openEmployeesTableFromDisc() {
        Employees employees = new Employees();

        try (ObjectInputStream oos = new ObjectInputStream(new FileInputStream("Employees.dat"))) {
            employees = (Employees) oos.readObject();
        } catch (Exception e) {
            System.out.println("Unexpected error occurred. Employees table does not opened.");
        }

        return employees;
    }
}
