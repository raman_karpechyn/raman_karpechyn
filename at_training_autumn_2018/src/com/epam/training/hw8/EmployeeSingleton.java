package com.epam.training.hw8;

public class EmployeeSingleton {
    private static Employees employees;

    private EmployeeSingleton() {
    }

    public static Employees getInstance() {
        if (employees == null) {
            employees = EmployeesService.openEmployeesTableFromDisc();
        }
        return employees;
    }
}
