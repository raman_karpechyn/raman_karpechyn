package com.epam.training.hw8;

import java.util.InputMismatchException;

public class Program {
    public static void main(String[] args) {
        EmployeeSingleton.getInstance();
        menu();
    }

    private static void menu() {
        System.out.println("Enter command:" +
                "\n1 - Add Employee" +
                "\n2 - Remove Employee" +
                "\n3 - Show Employees table" +
                "\n4 - Save Employees table to disc" +
                "\nexit - Close");

        String option = ScannerSingleton.getInstance().nextLine();

        switch (option) {
            case "1":
                EmployeesService.addEmployee(getIdFromConsole());
                break;
            case "2":
                EmployeesService.removeEmployee(getIdFromConsole());
                break;
            case "3":
                printEmployeesTable();
                break;
            case "4":
                EmployeesService.saveEmployeesTableToDisc();
                break;
            case "exit":
                return;
            default:
                System.out.println("Wrong command. Try again.");
                break;
        }

        menu();
    }

    private static int getIdFromConsole() {
        int id = -1;
        boolean flag = false;

        System.out.println("Enter Employee ID:");

        try {
            id = ScannerSingleton.getInstance().nextInt();
            flag = true;
        } catch (InputMismatchException e) {
            System.out.println("Incorrect ID!");
        } finally {
            ScannerSingleton.getInstance().nextLine();
        }

        if (flag) {
            return id;
        } else {
            return getIdFromConsole();
        }
    }

    private static void printEmployeesTable() {
        System.out.println(EmployeeSingleton.getInstance().toString());
    }
}
