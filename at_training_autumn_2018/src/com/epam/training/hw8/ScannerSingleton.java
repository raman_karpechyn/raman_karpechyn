package com.epam.training.hw8;

import java.util.Scanner;

public class ScannerSingleton {
    private static Scanner input;

    private ScannerSingleton() {
    }

    public static Scanner getInstance() {
        if (input == null) {
            input = new Scanner(System.in);
        }
        return input;
    }
}
