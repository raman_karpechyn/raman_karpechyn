package com.epam.training.hw4.task2;

public class SelectionSort implements Sorter {
    @Override
    public void sort(int[] array) {
        int temp;
        for (int i = 1, j; i < array.length; i++) {
            temp = array[i];
            for (j = i - 1; j >= 0 && array[j] > temp; j--) {
                array[j + 1] = array[j];
            }
            array[j + 1] = temp;
        }
    }
}
