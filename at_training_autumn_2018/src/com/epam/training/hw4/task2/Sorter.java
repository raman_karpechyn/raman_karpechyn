package com.epam.training.hw4.task2;

public interface Sorter {
    void sort(int[] array);
}
