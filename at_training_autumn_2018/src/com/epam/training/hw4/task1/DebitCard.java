package com.epam.training.hw4.task1;

import com.epam.training.hw3.task1.Card;

public class DebitCard extends Card {
    public DebitCard(String firstName, String middleName, String lastName, double balance) {
        super(firstName, middleName, lastName, balance);
    }

    public DebitCard(String firstName, String lastName, double balance) {
        super(firstName, lastName, balance);
    }

    public DebitCard(String firstName, String middleName, String lastName) {
        super(firstName, middleName, lastName);
    }

    public DebitCard(String firstName, String lastName) {
        super(firstName, lastName);
    }

    @Override
    public void decreaseBalance(double value) {
        if (getBalance() - value >= 0) {
            super.decreaseBalance(value);
        } else {
            throw new IllegalArgumentException("Argument value is more then balance value.");
        }
    }
}
