package com.epam.training.hw4.task1;

import com.epam.training.hw3.task1.Card;

public class AutomatedTellerMachine {
    private int atmID;

    public AutomatedTellerMachine(int atmID) {
        this.atmID = atmID;
    }

    public int getAtmID() {
        return atmID;
    }

    public void rechargeAccount(Card card, double value) {
        card.increaseBalance(value);
    }

    public void withdrawСash(Card card, double value) {
        card.decreaseBalance(value);
    }
}
