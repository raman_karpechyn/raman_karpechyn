package com.epam.training.hw4.task1;

import com.epam.training.hw3.task1.Card;

public class CreditCard extends Card {
    public CreditCard(String firstName, String middleName, String lastName, double balance) {
        super(firstName, middleName, lastName, balance);
    }

    public CreditCard(String firstName, String lastName, double balance) {
        super(firstName, lastName, balance);
    }

    public CreditCard(String firstName, String middleName, String lastName) {
        super(firstName, middleName, lastName);
    }

    public CreditCard(String firstName, String lastName) {
        super(firstName, lastName);
    }
}
