package com.epam.training.hw7.task1;

import java.util.Arrays;

public class Program {
    public static void main(String[] args) {
        Integer[] array = {-79, 9, 100, 27, 72, -13, 3, 34, 77, 7110, 101, 22, 1901, 25, 87, 8,
                62, 96, 51, 0, 80, -1, 12, -102, 36, 89, 69, -14, 97, 75};
        System.out.printf("Original array:\n%s\n", Arrays.toString(array));
        sortArrayBySum(array);
        System.out.printf("Sorted array:\n%s\n", Arrays.toString(array));
    }

    public static void sortArrayBySum(Integer[] array) {
        Arrays.sort(array, new SumSortComparator());
    }
}
