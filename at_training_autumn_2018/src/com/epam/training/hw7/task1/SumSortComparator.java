package com.epam.training.hw7.task1;

import java.util.Comparator;

public class SumSortComparator implements Comparator<Integer> {
    @Override
    public int compare(Integer o1, Integer o2) {
        if (o1 == null || o2 == null) {
            throw new IllegalArgumentException("Arguments are NULL.");
        }
        return getSumOfNumbers(o1) - getSumOfNumbers(o2);
    }

    private int getSumOfNumbers(int value) {
        int result = 0;
        for (; value != 0; value /= 10) {
            result += (value % 10);
        }
        return Math.abs(result);
    }
}
