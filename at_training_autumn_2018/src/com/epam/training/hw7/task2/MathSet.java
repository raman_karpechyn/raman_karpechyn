package com.epam.training.hw7.task2;

import java.util.HashSet;

public class MathSet<T> extends HashSet<T> {
    public boolean getUnion(MathSet<T> set) {
        return this.addAll(set);
    }

    public boolean getIntersection(MathSet<T> set) {
        return this.retainAll(set);
    }

    public boolean getComplement(MathSet<T> set) {
        return this.removeAll(set);
    }

    public boolean getSymmetricDifference(MathSet<T> set) {
        MathSet<T> temp = new MathSet<>();
        temp.addAll(this);

        this.getComplement(set);
        set.getComplement(temp);
        return this.getUnion(set);
    }
}
