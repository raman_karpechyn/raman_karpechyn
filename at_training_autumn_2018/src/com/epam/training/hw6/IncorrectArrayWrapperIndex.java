package com.epam.training.hw6;

public class IncorrectArrayWrapperIndex extends RuntimeException {
    public IncorrectArrayWrapperIndex() {
        super();
    }

    public IncorrectArrayWrapperIndex(String s) {
        super(s);
    }
}
