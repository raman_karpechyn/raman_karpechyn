package com.epam.training.hw6;

public class ArrayWrapper<T> {
    private T[] array;

    public ArrayWrapper(T[] array) {
        this.array = array;
    }

    public T get(int index) {
        checkIndex(index);

        return array[index - 1];
    }

    public boolean replace(int index, T value) {
        checkIndex(index);

        boolean flag = isValidValue(index, value);

        if (flag) {
            array[index - 1] = value;
        }

        return flag;
    }

    private boolean isValidValue(int index, T value) {
        if (value instanceof String && ((String) value).length() <= ((String) get(index)).length()) {
            return false;
        }
        if (value instanceof Integer && ((Integer) value) <= ((Integer) get(index))) {
            return false;
        }
        return true;
    }

    private void checkIndex(int index) {
        if (index <= 0 || index > array.length) {
            throw new IncorrectArrayWrapperIndex();
        }
    }
}
