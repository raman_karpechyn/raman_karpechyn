package com.epam.training.hw5;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class TextProcessor {
    public static Scanner input = new Scanner(System.in);

    public static String getTextFromConsole() {
        return input.nextLine();
    }

    public static String getTextFromRootFolder() {
        File rootFolder = new File(System.getProperty("user.dir"));
        File[] files = rootFolder.listFiles();

        if (files == null) {
            System.out.println("There ara no files in the root directory.");
            return Program.getText();
        }

        ArrayList<String> textFilesNames = new ArrayList<>(0);
        int i = 1;
        for (File textfiles : files) {
            if (textfiles.isFile() && textfiles.getName().endsWith(".txt")) {
                textFilesNames.add(String.format("%s - %s", i++, textfiles.getName()));
            }
        }

        if (textFilesNames.size() == 0) {
            System.out.println("There ara no text files in the root directory.");
            return Program.getText();
        }

        textFilesNames.add("0 - To go back");

        String fileName = getFileString(textFilesNames);

        if (fileName == null) {
            return Program.getText();
        }

        return readTextFromFile(fileName);
    }

    public static String getFileString(ArrayList<String> textFilesNames) {
        System.out.println("Choose file:");
        Program.printStringArrayList(textFilesNames);

        int fileNumber;
        try {
            fileNumber = input.nextInt();
        } catch (InputMismatchException ex) {
            System.out.println("Wrong file number. Try again.");
            input.next();
            return getFileString(textFilesNames);
        }

        if (fileNumber == 0) {
            return null;
        }

        try {
            return textFilesNames.get(fileNumber - 1).replace(String.format("%s - ", fileNumber), "");
        } catch (IndexOutOfBoundsException ex) {
            System.out.println("Wrong file number. Try again.");
        }
        return getFileString(textFilesNames);
    }

    public static String readTextFromFile(String fileName) {
        StringBuilder result = new StringBuilder();
        String temp;
        try (BufferedReader reader = Files.newBufferedReader(Paths.get(fileName))) {
            while ((temp = reader.readLine()) != null) {
                result.append(String.format("%s ", temp));
            }
        } catch (IOException ex) {
            System.out.println("Fail to read file.");
        }

        return result.toString();
    }

    public static ArrayList<String> processText(String text) {
        ArrayList<String> words = SentenceUtils.splitSentenceToWordsAndSortAlphabetically(text);
        SentenceUtils.removeAllDuplicatesCaseInsensitive(words);
        SentenceUtils.collectWordsByFirstCharacter(words);

        for (int i = 0; i < words.size(); i++) {
            words.set(i, String.format("%s: %s", SentenceUtils.getFirstCharInString(words.get(i)).toUpperCase(), words.get(i)));
        }

        return words;
    }
}
