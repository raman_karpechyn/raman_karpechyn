package com.epam.training.hw5;

import java.util.ArrayList;
import java.util.Arrays;

public class SentenceUtils {
    public static ArrayList<String> splitSentenceToWordsAndSortAlphabetically(String sentence) {
        ArrayList<String> temp = splitSentenceToWords(sentence);
        temp.sort(String.CASE_INSENSITIVE_ORDER);
        return temp;
    }

    public static ArrayList<String> splitSentenceToWords(String sentence) {
        return new ArrayList<>(Arrays.asList(removeAllPunctuators(sentence).split("\\s+")));
    }

    public static String removeAllPunctuators(String sentence) {
        return sentence.replaceAll("[^\\w\\s-']|\\B-\\B", "");
    }

    public static void removeAllDuplicatesCaseInsensitive(ArrayList<String> words) {
        for (int i = 0; i < words.size(); i++) {
            for (int j = 0; j < words.size(); j++) {
                if (i == j) {
                    continue;
                }
                if (words.get(i).equalsIgnoreCase(words.get(j))) {
                    words.remove(j);
                    j--;
                }
            }
        }
    }

    public static void collectWordsByFirstCharacter(ArrayList<String> words) {
        for (int i = 0; i < words.size(); i++) {
            for (int j = 0; j < words.size(); j++) {
                if (i == j) {
                    continue;
                }
                String a = getFirstCharInString(words.get(i));
                String b = getFirstCharInString(words.get(j));
                if (a.equalsIgnoreCase(b)) {
                    words.set(i, String.format("%s %s", words.get(i), words.get(j)));
                    words.remove(j);
                    j--;
                }
            }
        }
    }

    public static String getFirstCharInString(String str) {
        String result;
        if (str.length() == 0) {
            result = "";
        } else {
            result = Character.toString(str.charAt(0));
        }
        return result;
    }
}
