package com.epam.training.hw5;

import java.util.ArrayList;

public class Program {
    public static void main(String[] args) {
        String text = getText();

        if (text == null) {
            return;
        }

        ArrayList<String> words = TextProcessor.processText(text);
        System.out.println("\n\nResult:\n");
        printStringArrayList(words);
    }

    public static String getText() {
        System.out.println("Enter command:" +
                "\n1 - Manual input" +
                "\n2 - Choose file from root directory" +
                "\nexit - Close");

        String option = TextProcessor.getTextFromConsole();

        switch (option) {
            case "1":
                System.out.println("Enter text:");
                return TextProcessor.getTextFromConsole();
            case "2":
                return TextProcessor.getTextFromRootFolder();
            case "exit":
                return null;
            default:
                System.out.println("Wrong command. Try again.");
                break;
        }

        return getText();
    }

    public static void printStringArrayList(ArrayList<String> array) {
        for (String x : array) {
            System.out.println(x);
        }
    }
}
