package com.epam.training.hw2.task1;

public class Program {
    public static void main(String[] args) {
        System.out.println("Default values of Java primitives are:");
        System.out.printf("byte - %d%n", Initialiser.getByteDefaultValue());
        System.out.printf("short - %d%n", Initialiser.getShortDefaultValue());
        System.out.printf("char - %c%n", Initialiser.getCharDefaultValue());
        System.out.printf("int - %d%n", Initialiser.getIntDefaultValue());
        System.out.printf("long - %d%n", Initialiser.getLongDefaultValue());
        System.out.printf("float - %f%n", Initialiser.getFloatDefaultValue());
        System.out.printf("double - %f%n", Initialiser.getDoubleDefaultValue());
        System.out.printf("boolean - %s%n%n", Initialiser.getBooleanDefaultValue());

        System.out.println("Default value of all Java reference type is:");
        System.out.printf("Object - %s%n", Initialiser.getObjectDefaultValue());
        System.out.printf("Object - %s%n", Initialiser.getArrayDefaultValue());
    }
}
