package com.epam.training.hw2.task1;

public class Initialiser {
    private static byte byteDefaultValue;
    private static short shortDefaultValue;
    private static char charDefaultValue;
    private static int intDefaultValue;
    private static long longDefaultValue;

    private static float floatDefaultValue;
    private static double doubleDefaultValue;

    private static boolean booleanDefaultValue;

    private static Object objectDefaultValue;

    private static Object[] arrayDefaultValue;

    public static byte getByteDefaultValue() {
        return byteDefaultValue;
    }

    public static short getShortDefaultValue() {
        return shortDefaultValue;
    }

    public static char getCharDefaultValue() {
        return charDefaultValue;
    }

    public static int getIntDefaultValue() {
        return intDefaultValue;
    }

    public static long getLongDefaultValue() {
        return longDefaultValue;
    }

    public static float getFloatDefaultValue() {
        return floatDefaultValue;
    }

    public static double getDoubleDefaultValue() {
        return doubleDefaultValue;
    }

    public static boolean getBooleanDefaultValue() {
        return booleanDefaultValue;
    }

    public static Object getObjectDefaultValue() {
        return objectDefaultValue;
    }

    public static Object[] getArrayDefaultValue() {
        return arrayDefaultValue;
    }
}
