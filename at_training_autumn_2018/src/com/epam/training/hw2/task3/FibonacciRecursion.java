package com.epam.training.hw2.task3;

public class FibonacciRecursion {
    public static int getMaxFibonacciForInteger(int fibonacciFirst, int fibonacciSecond) {
        int fibonacciNumber = fibonacciFirst + fibonacciSecond;
        if (fibonacciNumber < 0) {
            return 1;
        }
        return 1 + getMaxFibonacciForInteger(fibonacciSecond, fibonacciNumber);
    }


    public static long getMaxFibonacciForLong(long fibonacciFirst, long fibonacciSecond) {
        long fibonacciNumber = fibonacciFirst + fibonacciSecond;
        if (fibonacciNumber < 0) {
            return 1;
        }
        return 1 + getMaxFibonacciForLong(fibonacciSecond, fibonacciNumber);
    }
}
