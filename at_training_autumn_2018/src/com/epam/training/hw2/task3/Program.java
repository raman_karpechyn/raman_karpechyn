package com.epam.training.hw2.task3;

public class Program {
    public static void main(String[] args) {
        System.out.println("Max Fibonacci number for Integer is");
        System.out.println(FibonacciRecursion.getMaxFibonacciForInteger(0, 1));

        System.out.printf("%nMax Fibonacci number for Long is%n");
        System.out.println(FibonacciRecursion.getMaxFibonacciForLong(0, 1));
    }
}
