package com.epam.training.hw2.task2;

public class Priority {
    private static Object staticVariables;
    private Object classVariables;


    {
        System.out.println("Non-static block");
    }

    {
        System.out.println("The Second non-static block");
    }


    static {
        System.out.println("Static block");
    }

    static {
        System.out.println("The second static block");
    }
}
